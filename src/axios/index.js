import axios from 'axios'
import qs from 'qs';
import store from '../store/index.js'
let arg = {}
let http = {};
http.post = function (api, params) {
  store.appStore.setLoading(false)
  let data = Object.assign({}, arg, params)
  return new Promise((resolve, reject) => {
    axios.post(api, data, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: [(data, headers) => {
          const datastr = qs.stringify(data);
          return datastr;
        }],
        timeout: 20000
      })
      .then((response) => {
        store.appStore.setLoading(false)
        resolve(response.data)
      })
      .catch((error) => {
        store.appStore.setLoading(false)
        reject(error.message)
      })
  })
}
http.$ajax = function (api, params) {
  store.appStore.setLoading(false)
  let data = Object.assign({}, arg, params)
  return new Promise((resolve, reject) => {
    axios.post(api, data, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: [(data, headers) => {
          const datastr = qs.stringify(data);
          return datastr;
        }]
      })
      .then((response) => {
        store.appStore.setLoading(false)
        resolve(response.data)
      })
      .catch((error) => {
        store.appStore.setLoading(false)
        reject(error.message)
      })
  })
}
http.fetch = function (api, params) {
  let data = params;
  return new Promise((resolve, reject) => {
    axios.post(api, data, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        transformRequest: [(data, headers) => {
          const datastr = qs.stringify(data);
          return datastr;
        }]
      })
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.message)
      })
  })
}
// http request 拦截器
axios.interceptors.request.use(
  config => {
    return config;
  },
  err => {
    return Promise.reject(err);
  });

// http response 拦截器
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = '请求错误(400)';
          break;
        case 401:
          error.message = '(401)';
          break;
        case 403:
          error.message = '拒绝访问(403)';
          break;
        case 404:
          error.message = '请求出错(404)';
          break;
        case 408:
          error.message = '请求超时(408)';
          break;
        case 500:
          error.message = '服务器错误(500)';
          break;
        case 501:
          error.message = '服务未实现(501)';
          break;
        case 502:
          error.message = '网络错误(502)';
          break;
        case 503:
          error.message = '服务不可用(503)';
          break;
        case 504:
          error.message = '网络超时(504)';
          break;
        case 505:
          error.message = 'HTTP版本不受支持(505)';
          break;
        default:
          error.message = `连接出错(${error.response.status})!`;
      }
    } else {
      error.message = '网络不稳定,请稍后重试'
    }
    return Promise.reject(error);
  });

export default http;
