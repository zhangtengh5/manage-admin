import React, {Component} from 'react';
import PrivateRoute from './router/router'
import {
  Route,
  Switch,
  HashRouter as Router,
} from 'react-router-dom'
import Login from './components/page/Login/index'
// import Login from './components/page/Login2/index'
import Index from './components/page/Index/index'
import './App.scss'
import './assets/font/iconfont.css'


class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/login' component={Login}/>
          <PrivateRoute path='/' component={Index}/>
        </Switch>
      </Router>
    )
  }
}

export default App;
