import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Popconfirm,
  Icon,
  Modal,
  Select
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'

import TypingCard from '../../common/TypingCard'
const FormItem = Form.Item;
const Option = Select.Option;
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'ID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: 'GID', width: 100, dataIndex: 'productId', key: 'productId'
    },
    {
      title: '商品名称',  dataIndex: 'productName', key: 'productName'
    },
    {
      title: '总价', width: 120, dataIndex: 'sum', key: 'sum'
    },
    {
      title: '数量', width: 120, dataIndex: 'amount', key: 'amount'
    },
    {
      title: '单价', width: 120, dataIndex: 'costPrice', key: 'costPrice'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {page, limit} = this.state;
    this.setState({isload: true})
    http.post('/orderItem/SelectOrderItem.do', {
      orderId: id,
      page: page,
      limit: limit
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={1000}
        destroyOnClose
        title="查看商品"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}

@Form.create() @inject('appStore') @observer
class CatTableU extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'ID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: '优惠券ID', width: 100, dataIndex: 'couponId', key: 'couponId'
    },
    {
      title: '优惠券编号', width: 150, dataIndex: 'couponNumber', key: 'couponNumber'
    },
    {
      title: '优惠券名字', dataIndex: 'name', key: 'name'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {page, limit} = this.state;
    this.setState({isload: true})
    http.post('/coupon/SelectOrderCoupon.do', {
      orderId: id,
      page: page,
      limit: limit
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="查看买类商品"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    catData: {},
    catVisible: false,
    catDataU: {},
    catVisibleU: false,
    isload: false,
    searchform: {
      account: '',
      payType: ''
    },
    localBtns: [{
      id: 62,
      name: '导出',
      isshow: false
    }, {
      id: 63,
      name: '退款',
      isshow: false
    }, {
      id: 61,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '手机号', dataIndex: 'account', key: 'account', width: 120},
    {title: '订单编号', dataIndex: 'orderNum', key: 'orderNum', width: 280},
    {title: '订单状态', dataIndex: 'orderStatusName', key: 'orderStatusName', width: 120},
    {
      title: '付款状态',
      dataIndex: 'payStatus',
      key: 'payStatus',
      width: 100,
      render: (text, record) => (
        record.payStatus === 2 ? '已支付' : '未支付'
      )
    },
    {title: '订单总额', dataIndex: 'sum', key: 'sum', width: 100},
    {title: '折扣金额', dataIndex: 'discountMoney', key: 'discountMoney', width: 100},
    {title: '支付方式', dataIndex: 'payTypeName', key: 'payTypeName', width: 120},
    {
      title: '配送方式',
      dataIndex: 'deliverType',
      key: 'deliverType',
      width: 100,
      render: (text, record) => {
        if (record.deliverType === 1) {
          //1 自提 2. 送货上门 3 线下购买
          return '自提'
        } else if (record.deliverType === 2) {
          return '送货上门'
        } else if (record.deliverType === 3) {
          return '线下购买'
        } else {
          return ''
        }
      }
    },
    {title: '订单积分', dataIndex: 'points', key: 'points', width: 120},
    {title: '下单时间', dataIndex: 'createTime', key: 'createTime',width: 200},
    {title: '店铺名称', dataIndex: 'storeName', key: 'storeName', width: 200},
    {title: '设备名称', dataIndex: 'equipmentname', key: 'equipmentname', width: 120},
    {title: '支付流水号', dataIndex: 'payOrdernum', key: 'payOrdernum', width: 150},
    {title: '完成时间', dataIndex: 'completeAt', key: 'completeAt',width: 200},
    {title: '收货号码', dataIndex: 'telephone', key: 'telephone', width: 120},
    {title: '收货人', dataIndex: 'consignee', key: 'consignee', width: 120},
    {
      title: '收货地址',
      dataIndex: 'provinceName',
      key: 'provinceName',
      width: 250,
      render: (text, record) => {
        if (record.provinceName) {
          return record.provinceName + '省' + record.districtName + record.blockName + record.addressDetail
        }
        return ''
      }
    },
    {title: '备注', dataIndex: 'remark', key: 'remark', width: 120},
    {title: '取消备注', dataIndex: 'cancelRemark', key: 'cancelRemark', width: 120},
    {
      title: '订单商品',
      key: 'cat',
      width: 90,
      fixed: 'right',
      align: 'center',
      render: (text, record) => {
        return <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
      },
    },
    {
      title: '订单优惠',
      key: 'catU',
      width: 90,
      fixed: 'right',
      align: 'center',
      render: (text, record) => {
        if (record.isUse === 'true') {
          return <Button size="small" onClick={() => this.handleCatU(record)}><Icon type="search"/></Button>
        }
        return <Button size="small" disabled><Icon type="search"/></Button>
      },
    },
    {
      title: '操作',
      key: 'operation',
      width: 80,
      align: 'center',
      fixed: 'right',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          if (record.orderStatus === 2 || record.orderStatus === 3) {
            return (
              <span>
                <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">退款</Button>
                </Popconfirm>
              </span>
            )
          } else {
            return <Button type="danger" size="small" disabled>退款</Button>
          }
        } else {
          return <Button type="danger" size="small" disabled>退款</Button>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="手机号">
              {getFieldDecorator('account', {
                initialValue: ''
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem label="支付方式">
              {getFieldDecorator('payType', {
                initialValue: ""
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value="">请选择支付方式</Option>
                  <Option value={1}>支付宝</Option>
                  <Option value={2}>微信</Option>
                  <Option value={3}>掌购币</Option>
                  <Option value={4}>银行卡</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      account: searchform.account,
      payType: searchform.payType
    }
    this.setState({isload: true})
    http.post('/order/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  handleCatU(rows) {
    this.setState({
      catVisibleU: true,
      catDataU: rows
    });
  }
  closeCatU = () => {
    this.setState({
      catVisibleU: false,
      catDataU: []
    });
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/alipaytool/cancelOrder.do', {
      adminId: adminId,
      orderNum: rows.orderNum
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleExport () {
    const { storeId } = this.props.appStore.loginUser;
    window.location.href = '/order/downloadOrder.do?storeId=' + storeId + '&_time=' + (new Date()).getTime();
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
        console.log(localBtns)
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      catVisible,
      catData,
      catVisibleU,
      catDataU
    } = this.state;
    const catMethods = {
      handleVisible: this.closeCat
    };
    const catMethodsU = {
      handleVisible: this.closeCatU
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['订单管理', '订单列表']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Popconfirm title='确定要导出么？' onConfirm={() => (this.handleExport())} okText="Yes" cancelText="No">
              <Button type="primary" icon="download" style={{display: (this.state.localBtns[0].isshow) ? 'block': 'none'}}>导出</Button>
            </Popconfirm>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            scroll={{x: 3000}}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
        {catDataU && Object.keys(catDataU).length ? (
          <CatTableU
            {...catMethodsU}
            modalVisible={catVisibleU}
            values={catDataU}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage