import React, { Component } from 'react';
import {
  Card,
  BackTop,
  Form,
  Button,
  message,
  Upload, 
  Icon
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import './index.scss'
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}
@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    imageUrl: '',
    loading: false
  }
  handleChange = (info) => {
    //console.log(info.file)
    if (info.file.status === 'uploading') {
      this.setState({
        loading: true
      });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      if (info.file.response.status === 1) {
        getBase64(info.file.originFileObj, () => {
          this.setState({
            loading: false
          }, () => {
            this.setState({
              imageUrl: info.file.response.path
            })
            message.success(info.file.response.msg);
          })
        });
      } else {
        message.error(info.file.response.msg);
      }
    }
  }
  gotTableData () {
    const {imageUrl} = this.state;
    http.post('/conmon/uploadCode.do', {
      appleCode: imageUrl,
      androidCode: ''
    })
    .then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentDidMount() {}
  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const {imageUrl} = this.state;
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['系统管理','APP版本管理']}/>
        <Card bordered={true} title='上传APP二维码' style={styles}>
          <div style={{display:'flex', justifyContent: 'center',margin: '30px 0'}}>
            <Upload
              style={{width: "128px",height: "128px"}}
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action = "/conmon/uploadPc.do"
              name = "file"
              onChange={this.handleChange}
            >
              {imageUrl ? <img src={imageUrl} alt="avatar" style={{width: '128px', height: '128px'}}/> : uploadButton}
            </Upload>
          </div>
          <div style={{textAlign: 'center'}}>
            <Button type="primary" onClick={() => this.gotTableData()}>上传</Button>
          </div> 
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
      </div>
    )
  }
}

const styles = {
  height: 'calc(100vh - 240px)'
}

export default TablePage