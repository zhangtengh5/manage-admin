import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Popconfirm,
  Icon,
  Modal,
  Radio,
  InputNumber
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'

import TypingCard from '../../common/TypingCard'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
@Form.create() @inject('appStore') @observer
class EditForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {}
  }
  okHandle = () => {
    const { form, handleSave, values} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const data = {
        ...fieldsValue,
        id: values.id
      }
      handleSave(data);
    });
    
  };
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
      destroyOnClose
      width={600}
      title="修改积分"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }} style={{marginBottom: '20px'}}>
          <Col span={12}>
            <FormItem label="积分操作">
              {getFieldDecorator('doType',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                < RadioGroup>
                  <Radio value={1}>增加</Radio>
                  <Radio value={2}>减少</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }} style={{marginBottom: '20px'}}>
          <Col span={24}>
            <FormItem label="积分数量">
              {getFieldDecorator('integral',{
                rules: [{ required: true, message: '请输入正整数!'}],
              })(
                <InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '200px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
     </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'ID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: '积分数量', width: 150, dataIndex: 'integralNum', key: 'integralNum'
    },
    {
      title: '获取方式', width: 120, dataIndex: 'typeName', key: 'typeName'
    },
    {
      title: '用户ID', width: 120, dataIndex: 'userId', key: 'userId'
    },
    {
      title: '订单ID', width: 120, dataIndex: 'orderId', key: 'orderId'
    },
    {
      title: '获取时间', dataIndex: 'createTime', key: 'createTime'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {page, limit} = this.state;
    this.setState({isload: true})
    http.post('/integral/SelectByUserId.do', {
      userId: id,
      page: page,
      limit: limit
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={860}
        destroyOnClose
        title="查看积分"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    catData: {},
    catVisible: false,
    editVisible: false,
    editData: {},
    isload: false,
    searchform: {
      account: '',
      payType: ''
    },
    localBtns: [{
      id: 69,
      name: '导出',
      isshow: false
    }, {
      id: 70,
      name: '积分修改',
      isshow: false
    }, {
      id: 71,
      name: '删除',
      isshow: false
    }, {
      id: 72,
      name: '清除掌脉',
      isshow: false
    }, {
      id: 68,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'UID', dataIndex: 'id', key: 'id', width: 80},
    {title: '注册时间', dataIndex: 'createAt', key: 'createAt',width: 200},
    {title: '手机号', dataIndex: 'account', key: 'account', width: 120},
    {title: '是否绑定掌静脉', dataIndex: 'isZVein', key: 'isZVein', width: 100},
    {title: '掌购币', dataIndex: 'handMoney', key: 'handMoney', width: 150},
    {title: '积分', dataIndex: 'integral', key: 'integral',width: 120},
    {title: '注册门店', dataIndex: 'storeName', key: 'storeName'},
    {title: '用户名', dataIndex: 'name', key: 'name', width: 120},
    {title: '性别', dataIndex: 'sex', key: 'sex', width: 120},
    {title: '生日', dataIndex: 'birthday', key: 'birthday', width: 120},
    {title: '会员类型', dataIndex: 'associatorName', key: 'associatorName', width: 120},
    {title: '推荐人', dataIndex: 'recPhone', key: 'recPhone', width: 120},
    {
      title: '积分',
      key: 'cat',
      width: 120,
      fixed: 'right',
      align: 'center',
      render: (text, record) => {
        if (this.state.localBtns[1].isshow) {
          return (
            <span>
              <Button size="small" style={{marginRight: '10px'}} onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
              <Button size="small" onClick={() => this.handleEdit(record)}><Icon type="edit"/></Button>
            </span>
          )
        } else {
          return (
            <span>
              <Button size="small" style={{marginRight: '10px'}} onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
              <Button size="small" disabled><Icon type="edit"/></Button>
            </span>
          )
        }
      },
    },
    {
      title: '操作',
      key: 'operation',
      width: 180,
      align: 'center',
      fixed: 'right',
      render: (text,record) => {
        if (this.state.localBtns[2].isshow && this.state.localBtns[3].isshow) {
          if (record.isZVein === '已绑定') {
            return (
              <span>
                <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small" style={{marginRight: '10px'}}>删除</Button>
                </Popconfirm>
                <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDelClear(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">清除掌脉</Button>
                </Popconfirm>
              </span>
            )
          } else {
            return (
              <span>
                <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small" style={{marginRight: '10px'}}>删除</Button>
                </Popconfirm>
                <Button type="danger" size="small" disabled>清除掌静脉</Button>
              </span>
            )
          }
        } else if (this.state.localBtns[2].isshow && !this.state.localBtns[3].isshow) {
          return <span>
            <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small" style={{marginRight: '10px'}}>删除</Button>
            </Popconfirm>
            <Button type="danger" size="small" disabled>清除掌脉</Button>
          </span>
        } else if (!this.state.localBtns[2].isshow && this.state.localBtns[3].isshow) {
          if (record.isZVein === '已绑定') {
            return (
              <span>
                <Button type="danger" size="small" style={{marginRight: '10px'}} disabled>删除</Button>
                <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDelClear(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">清除掌脉</Button>
                </Popconfirm>
              </span>
            )
          } else {
            return (
              <span>
                <Button type="danger" size="small" style={{marginRight: '10px'}} disabled>删除</Button>
                <Button type="danger" size="small" disabled>清除掌静脉</Button>
              </span>
            )
          }
        } else {
           return <span>
            <Button type="danger" size="small" style={{marginRight: '10px'}} disabled>删除</Button>
            <Button type="danger" size="small" disabled>清除掌脉</Button>
          </span>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="手机号">
              {getFieldDecorator('account', {
                initialValue: ''
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      limit: limit,
      account: searchform.account
    }
    this.setState({isload: true})
    http.post('/userInfo/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  handleEdit = (rows) => {
    this.setState({
      editVisible: true,
      editData: rows
    });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false
    });
  }
  saveEdit = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/userInfo/UpdateIntegral.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/userInfo/DeleteUser.do', {
      adminId: adminId,
      id: rows.id,
      phone: rows.account
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDelClear = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/SalesPromotion/DeleteUser.do', {
      adminId: adminId,
      phone: rows.account
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleExport () {
    window.location.href = '/userInfo/downloadUserInfo.do?_time=' + (new Date()).getTime();
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      catVisible,
      catData,
      editVisible,
      editData
    } = this.state;
    const catMethods = {
      handleVisible: this.closeCat
    };
    const editMethods = {
      handleVisible: this.closeEdit,
      handleSave: this.saveEdit
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['会员管理', '会员列表']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Popconfirm title='确定要导出么？' onConfirm={() => (this.handleExport())} okText="Yes" cancelText="No">
              <Button type="primary" icon="download" style={{display: (this.state.localBtns[0].isshow) ? 'block': 'none'}}>导出</Button>
            </Popconfirm>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            scroll={{x: 1800}}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
        {editData && Object.keys(editData).length ? (
          <EditForm
            {...editMethods}
            modalVisible={editVisible}
            values={editData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage