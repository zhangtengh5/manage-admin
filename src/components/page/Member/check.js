import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Form,
  Row,
  Col,
  Button,
  message,
  Icon,
  Tag,
  Upload
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    page2: 1,
    limit2: 10,
    total2: 0,
    tableData2: [],
    loading: false,
    newMoney: '0.00',
    oldMoney: '0.00',
    msg: '',
    path: '',
    status: 1
  }
  columns = [
    {title: '时间', dataIndex: 'time', key: 'time',width: 200},
    {title: '商户订单', dataIndex: 'orderNo', key: 'orderNo'},
    {title: '金额', dataIndex: 'money', key: 'money', width: 90},
    {title: '订单状态', dataIndex: 'orderName', key: 'orderName', width: 90}
  ];
  columns2 = [
    {title: '时间', dataIndex: 'time', key: 'time',width: 200},
    {title: '商户订单', dataIndex: 'orderNo', key: 'orderNo'},
    {title: '金额', dataIndex: 'money', key: 'money', width: 90},
    {title: '订单状态', dataIndex: 'orderName', key: 'orderName', width: 90},
    {
      title: '对账结果',
      dataIndex: 'check',
      key: 'check',
      width: 90,
      render: (text, record) => {
        if (record.status === 1) {
          return '正常'
        } else {
          return '异常'
        }
      }
    }
  ];
  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      //console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} 文件上传成功`);
      this.handleData(info.file.response)
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 文件上传失败`);
    }
  }
  beforeUpload = (file) => {
    if (file.name.indexOf('.xlsx') === -1) {
      message.error('请上传.xlsx格式账单文件!');
      return false
    } else {
      let param = new FormData();
      param.append('file', file);      
      http.fetch('/StatementCentre/upload.do', param)
      .then((result) => {
        message.success(`${file.name} 文件上传成功`);
        this.handleData(result)
      })
      .catch((error) => {
        message.error(error)
      })
    }
  }
  handleData (option) {
    this.setState({
      tableData: option.oldData,
      total: option.oldData.length,
      tableData2: option.newData,
      total2: option.newData.length,
      newMoney: option.newMoney,
      oldMoney: option.oldMoney,
      msg: option.msg,
      path: option.path,
      status: option.status
    })
  }
  handleExport = () => {
    if (this.state.path) {
      window.location.href = this.state.path + '?_time=' + (new Date()).getTime();
    } else {
      message.error('对账可能有误，请重新对账');
    }
  }
  
  componentDidMount() {
    
  }
  render() {
    const {
      tableData,
      total,
      limit,
      tableData2,
      total2,
      limit2,
      newMoney,
      oldMoney,
      msg,
      status
    } = this.state;
    
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['订单管理', '流水对账']}/>
        <Card bordered={true} title='流水对账' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card" style={{textAlign:'left'}}>
              <Upload
                beforeUpload = {this.beforeUpload}
                showUploadList={false}
              >
                <Button>
                  <Icon type="upload" /> 上传账单
                </Button>
              </Upload>
              {
                status=== 2 && <Button type="primary" icon="download" onClick={this.handleExport}>导出</Button>
              }
              {
                msg && <Tag color="#f50" style={{marginLeft: '30px'}}>{msg}</Tag>
              }
              
          </div>
          <div className="table-box">
            <Row>
              <Col span={24}>
                <Tag color="blue">第三方账单</Tag>
                <Tag color="green" style={{marginLeft: '20px'}}>合计金额：{oldMoney}</Tag>
                <Table 
                  rowKey= "time"
                  dataSource={tableData} 
                  columns={this.columns}
                  style={styles.tableStyle}
                  bordered
                  scroll={{ y: 400 }}
                  pagination = {
                    {
                      showSizeChanger: true,
                      showQuickJumper: true,
                      pageSize: limit,
                      pageSizeOptions: ['10', '30', '50'],
                      total: total,
                      showTotal: function (total) {
                        return `共 ${total} 条`
                      }
                    }
                  }
                  onChange = { (pagination) => {
                    this.setState({
                      page: pagination.current,
                      limit: pagination.pageSize
                    })
                  }}   
                ></Table>
              </Col>
            </Row>
            <Row style={{marginTop: '30px'}}>
              <Col span={24}>
                <Tag color="blue">掌购账单</Tag>
                <Tag color="green" style={{marginLeft: '20px'}}>合计金额：{newMoney}</Tag>
                <Table 
                  rowKey = "time"
                  dataSource={tableData2} 
                  columns={this.columns2}
                  style={styles.tableStyle}
                  bordered
                  scroll={{ y: 400 }}
                  pagination = {
                    {
                      showSizeChanger: true,
                      showQuickJumper: true,
                      pageSize: limit2,
                      pageSizeOptions: ['10', '30', '50'],
                      total: total2,
                      showTotal: function (total) {
                        return `共 ${total} 条`
                      }
                    }
                  }
                  onChange = { (pagination) => {
                    this.setState({
                      page: pagination.current,
                      limit: pagination.pageSize
                    })
                  }}   
                ></Table>
              </Col>
            </Row>
          </div>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%',
    marginTop: '10px'
  }
}

export default TablePage