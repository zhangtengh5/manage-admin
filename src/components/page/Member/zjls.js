import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Popconfirm,
  Icon,
  Select,
  DatePicker
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'

import TypingCard from '../../common/TypingCard'
const FormItem = Form.Item;
const Option = Select.Option;


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    expandForm: false,
    storeList: [],
    searchform: {},
    localBtns: [{
      id: 65,
      name: '导出',
      isshow: false
    },{
      id: 64,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '流水时间', dataIndex: 'createTime', key: 'createTime',width: 200},
    {title: '流水号', dataIndex: 'transactionId', key: 'transactionId',width: 200},
    {title: '手机号', dataIndex: 'account', key: 'account', width: 120},
    {title: '金额', dataIndex: 'money', key: 'money', width: 150},
    {title: '付款类型', dataIndex: 'payTypeName', key: 'payTypeName',width: 120},
    {title: '进账类型', dataIndex: 'addTypeName', key: 'addTypeName', width: 150},
    {title: '掌购币余额', dataIndex: 'remainMoney', key: 'remainMoney', width: 150},
    {title: '所属门店', dataIndex: 'storeName', key: 'storeName',width: 200},
    {title: '设备名称', dataIndex: 'equipmentName', key: 'equipmentName',width: 120},
  ];
  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="手机号">
              {getFieldDecorator('account', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="流水号">
              {getFieldDecorator('serial', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <span style={{ marginLeft: 8, color: '#1890ff'}} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </span>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }
  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    const {storeList} = this.state;
    const { storeId } = this.props.appStore.loginUser;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="手机号" style={{marginLeft: '14px'}}>
              {getFieldDecorator('account', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="流水号" style={{marginLeft: '14px'}}>
              {getFieldDecorator('serial', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="所属店铺">
              {getFieldDecorator('storeId', {
                initialValue: storeId
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={0} key={0}>全部门店</Option>
                  {
                    storeList.map((item, index) => <Option value={item.id} key={index+1}>{item.name}</Option>)
                  }
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('beginTime', {
                
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择" style={{ width: '200px' }}/>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime', {
                
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择" style={{ width: '200px' }}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', margin: "20px 0" }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <span style={{ marginLeft: 8, color: '#1890ff'}} onClick={this.toggleForm}>
              收起 <Icon type="up"/>
            </span>
          </div>
        </div>
      </Form>
    );
  }
  toggleForm = () => {
    this.setState((prevState, props) => ({
      expandForm: !prevState.expandForm
    }))
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let values = {}
      if (fieldsValue.beginTime && !fieldsValue.endTime) {
        values = {
          ...fieldsValue,
          beginTime: fieldsValue['beginTime'].format('YYYY-MM-DD'),
          endTime: ""
        }
      } else if (!fieldsValue.beginTime && fieldsValue.endTime) {
        values = {
          ...fieldsValue,
          beginTime: "",
          endTime: fieldsValue['endTime'].format('YYYY-MM-DD')
        }
      } else if (fieldsValue.beginTime && fieldsValue.endTime) {
        values = {
          ...fieldsValue,
          beginTime: fieldsValue['beginTime'].format('YYYY-MM-DD'),
          endTime: fieldsValue['endTime'].format('YYYY-MM-DD')
        }
      } else {
        values = {
          ...fieldsValue,
          beginTime: "",
          endTime: ""
        }
      }
      this.setState({
        searchform: values
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId} = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      ...searchform
    }
    this.setState({isload: true})
    http.post('/recharge/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  gotStoreList () {
    http.post('/store/SelectAllStore.do')
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          storeList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleExport () {
    const { searchform } = this.state;
    const account = searchform.account || '';
    const serial = searchform.serial || '';
    const beginTime = searchform.beginTime || '';
    const endTime = searchform.endTime || '';
    const storeId = searchform.storeId;
    window.location.href = '/recharge/downloadRecharge.do?_time=' + (new Date()).getTime() + 
    '&serial=' + serial + 
    '&account=' + account + 
    '&beginTime=' + beginTime + 
    '&endTime' + endTime +
    '&storeId' + storeId
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotStoreList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload
    } = this.state;
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['订单管理', '资金流水']}/>
        <TypingCard source={this.renderForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Popconfirm title='确定要导出么？' onConfirm={() => (this.handleExport())} okText="Yes" cancelText="No">
              <Button type="primary" icon="download" style={{display: (this.state.localBtns[0].isshow) ? 'block': 'none'}}>导出</Button>
            </Popconfirm>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData}
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage