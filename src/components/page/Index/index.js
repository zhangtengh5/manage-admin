import React from 'react'
import { withRouter } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Spin,Layout} from 'antd'
import SiderNav from '../../common/SiderNav'
import ContentMain from '../../../router/routes'
import HeaderBar from '../../common/HeaderBar'
const {Sider, Header, Content, Footer} = Layout

@withRouter @inject('appStore') @observer
class Index extends React.Component{
  state = {
    collapsed: false
  }
  componentWillMount () {
    
  }
  componentDidMount () {
   
  }
  componentDidUpdate () {
    
  }
  toggle = () => {
    // console.log(this)  状态提升后，到底是谁调用的它
    this.setState({
      collapsed: !this.state.collapsed
    })
  }
  render() {
    // 设置Sider的minHeight可以使左右自适应对齐
    const { appStore } = this.props;
    return (
      <div id='page'>
        <Layout>
          <Sider 
           collapsible
           trigger={null}
           collapsed={this.state.collapsed}
          >
            <SiderNav/>
          </Sider>
          <Layout>
            <Header style={{background: '#fff', padding: '0 16px'}}>
              <HeaderBar collapsed={this.state.collapsed} onToggle={this.toggle}/>
            </Header>
            <Content>
              <ContentMain/>
            </Content>
            <Footer style={{textAlign: 'center'}}>ZhangGouBianLi ©2018 Created by 掌腾信息</Footer>
          </Layout>
        </Layout>
        <div className="load-box" style={appStore.isload ? styles.showload : styles.hideload}>
          <Spin size = "large" />
        </div>
      </div>
    );
  }
}
const styles = {
  showload: {
    display: 'flex'
  },
  hideload: {
    display: 'none'
  }
}
export default Index