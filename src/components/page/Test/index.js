import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  InputNumber,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  Select,
  message,
  Modal,
  Alert,
  Popconfirm,
  Radio,
} from 'antd'
import { inject, observer } from 'mobx-react'
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import TypingCard from '../../common/TypingCard'
import SingleUpload from '../../common/Upload/index'
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="创建RFID"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <Form layout="inline">
        <FormItem label="RFID数量">
          {form.getFieldDecorator('number', {
            initialValue: '',
            rules: [{ required: true, message: '请输入正整数!'}],
          })(<InputNumber 
              min={1} 
              placeholder = "请输入数量" 
              allowClear 
              style={{width: '200px'}}
              formatter = {
                (value) => {
                  if (value && !isNaN(value)) {
                    return parseInt(value)
                  } else {
                    return ''
                  }
                }
              }
            />
          )}
        </FormItem>
      </Form>
    </Modal>
  );
});
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleCatTableVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    console.log(props)
    this.state = {
      page: 1,
      limit: 10,
      total: 0,
      tableData: [],
      isload: false
    }
  }
  tipsMessage () {
    return (
      <div>
        <p>请输入将要导出的RFID开始流水号，已导出的RIFD流水号请勿重复导出。</p>
        <p>如：已有RFID流水号20个，输入5时，将导出5~20流水号的RFID号</p>
      </div>
    )
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 100},
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 250},
    {title: '流水号', dataIndex: 'serial', key: 'serial', width: 200},
    {title: 'RFID编号', dataIndex: 'rfid', key: 'rfid',width: 250},
    {title: '商品ID', dataIndex: 'proId', key: 'proId', width: 100},
    {title: '商品名称', dataIndex: 'proName', key: 'proName', width: 250},
  ];
  gotTableData() {
    const { adminId } = this.props.appStore.loginUser;
    const { id } = this.props.values;
    const {limit, page} = this.state;
    this.setState({isload: true})
    http.post('/rfid/SelectByProId.do', {
      adminId: adminId,
      proId: id,
      limit: limit,
      page: page
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const { adminId } = this.props.appStore.loginUser;
    const { id } = this.props.values;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const num = values.num;
        const date = new Date();
        window.location.href = "/rfid/downloadRFID.do?proId=" + id + '&num=' + num + '&adminId=' + adminId + '&_time=' + date.getTime();
      }
    });
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { limit, total, tableData, isload } = this.state;
    const { modalVisible, handleCatTableVisible } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
        width={1000}
        destroyOnClose
        title="数据展示"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleCatTableVisible()}
      >
        <div>
          <Form layout="inline" onSubmit={this.handleSubmit}>
            <FormItem label="流水号">
              {getFieldDecorator('num', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!' }],
              })(<InputNumber 
                    min={1} 
                    placeholder = "请输入" 
                    allowClear 
                    style={{width: '200px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
            </FormItem>
            <FormItem>
              <Button type="primary" htmlType="submit" icon="download">
                导出
              </Button>
            </FormItem>
          </Form>
        </div>
        <Alert
          message="注意"
          description = {this.tipsMessage()}
          type="error"
          style={{margin: "15px 0"}}
        />
        <Table 
          rowKey= "id"
          dataSource={tableData} 
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = { (pagination) => {
            this.setState({
              page: pagination.current,
              limit: pagination.pageSize
            }, () => {
              this.gotTableData();
            })
          }}
                
        ></Table>
      </Modal>
    )
  }
}

class GoodsTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {}
  }
  componentDidMount () {
    
  }
  render () {
    const { modalVisible, handleVisible, values} = this.props;
    console.log(values)
    return (
      <Modal
        width={375}
        destroyOnClose
        title="数据展示"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <div className= 'content-box' dangerouslySetInnerHTML={{__html: values.introduction}}>

        </div>
      </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      topPath: '',
      introduction: '',
      editorState: EditorState.createEmpty(),
      contentState: '666666666666'
    }
  }
  gotImage = (path) => {
    this.setState({
      topPath: path
    })
  }
  onEditorStateChange = (editorState) => {
    console.log(editorState)
    this.setState({
      editorState,
    });
  }
  onContentStateChange = (contentState) => {
    console.log(contentState)
    this.setState({
      contentState,
    });
  }
  uploadImageCallBack = (file) => {
    

  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { topPath, introduction} = this.state;
    const { id } = this.props.editValues;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!topPath) {
        message.warning('详情头图不能为空');
        return
      } else if (!introduction) {
        message.warning('详情内容不能为空');
        return
      } else {
        const values = {
          ...fieldsValue,
          topPath: topPath,
          introduction: introduction,
          id: id
        }
        handleSave(values);
      }
    });
  }
  componentDidMount () {
    const { topPath, introduction} = this.props.editValues;
    this.setState({
      topPath: topPath,
      introduction: introduction
    })
    console.log(introduction)
  }
  render () {
    const { modalVisible, handleVisible, editValues, categoryList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { topPath, introduction} = this.state;
    const { editorState,contentState } = this.state;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="商品分类">
                {getFieldDecorator('categoryId',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.categoryId
                })(
                  <Select placeholder="请选择" style={{ width: '200px' }}>
                    {
                      categoryList.map((item, index) => <Option value={item.id} key={index}>{item.name}</Option>)
                    }
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="是否新品">
                {getFieldDecorator('isNew',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.isNew
                })(
                  <RadioGroup>
                    <Radio value={0}>新品</Radio>
                    <Radio value={1}>否</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="是否上架">
                {getFieldDecorator('status',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.status
                })(
                  <RadioGroup>
                    <Radio value={1}>上架</Radio>
                    <Radio value={0}>下架</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="是否推荐">
                {getFieldDecorator('isrecommd',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.isrecommd
                })(
                  <RadioGroup>
                    <Radio value={1}>推荐</Radio>
                    <Radio value={0}>否</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="详情链接">
                {getFieldDecorator('linkUrl',{
                  rules: [{ required: true}],
                  initialValue: editValues.linkUrl
                })(
                  <Input placeholder = "请输入" style={{width: '630px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
           <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="虚拟库存">
                {getFieldDecorator('stock',{
                  rules: [{ required: true, message: '请输入正整数!' }],
                  initialValue: editValues.stock
                })(
                  <InputNumber 
                    placeholder = "请输入"
                    style = {{width: '200px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="实际库存">
                {getFieldDecorator('faskStock',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.faskStock
                })(
                  <InputNumber placeholder = "请输入" disabled style={{width: '200px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="详情头图" required>
                <SingleUpload callback={this.gotImage} imageUrl = {topPath}/>
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="详情内容" required>
                <Editor
                  editorState={editorState}
                  onEditorStateChange={this.onEditorStateChange}
                  onContentStateChange={this.onContentStateChange}
                  editorClassName="editor-class"
                  toolbarClassName="toolbar-class"
                  localization={{ locale: 'zh'}}
                  toolbar={{
                    image: { 
                      uploadCallback: this.uploadImageCallBack, 
                      alt: { present: true, mandatory: true }
                    },
                  }}
                />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    expandForm: false,
    searchform: {
      status: '',
      type: 0,
      invcode: "",
      categoryId: 0,
      productName: ""
    },
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    categoryList: [],
    modalVisible: false,
    catTableVisible: false,
    catData: {},
    typeload: false,
    goodsload: false,
    editData: {},
    editVisible: false,
    goodsDetail: {},
    goodsVisible: false
  }
  columns = [
    {title: 'GID', dataIndex: 'id', key: 'id', width: 80},
    {title: '商品标识码', dataIndex: 'invcode', key: 'invcode',width: 250},
    {title: '商品名称', dataIndex: 'productName', key: 'productName', width: 250},
    {
      title: '商品图片',
      dataIndex: 'path',
      key: 'path',
      width: 40,
      align: 'center',
      render: (path) => (
        <img src={path} style={{width: "40px", height: "22px"}} alt=""/>
      )
    },
    {title: '商品类型', dataIndex: 'categoryName', key: 'categoryName', width: 200},
    {title: '原价', dataIndex: 'cksaleprice', key: 'cksaleprice', width: 100},
    {title: '售价', dataIndex: 'posprice', key: 'posprice', width: 100},
    {title: '实际库存', dataIndex: 'faskStock', key: 'faskStock', width: 150},
    {title: '虚拟库存', dataIndex: 'stock', key: 'stock', width: 150},
    {
      title: '商品状态', 
      dataIndex: 'status', 
      key: 'status', 
      width: 100,
      align: 'center',
      render: (status) => (
        status === 1 ? '上架' : '下架'
      )
    },
    {
      title: '是否促销', 
      dataIndex: 'isrecommd',
      key: 'isrecommd',
      width: 100,
      align: 'center',
      render: (isrecommd) => (
        isrecommd === 1 ? '是' : '否'
      )
    },
    {
      title: '是否新品',
      dataIndex: 'isNew',
      key: 'isNew',
      width: 100,
      align: 'center',
      render: (isNew) => (
        isNew === 1 ? '是' : '否'
      )
    },
    {title: '收藏量', dataIndex: 'collctNum', key: 'collctNum', width: 150},
    {title: '点赞量', dataIndex: 'upvoteNum', key: 'upvoteNum', width: 150},
    {title: '规格', dataIndex: 'std', key: 'std', width: 150},
    {title: '单位', dataIndex: 'cunit', key: 'cunit', width: 150},
    {title: '品牌', dataIndex: 'brand', key: 'brand', width: 150},
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 300},
    {title: '更新时间', dataIndex: 'updateTime', key: 'updateTime', width: 300},
    {
      title: 'RFID操作',
      key: 'rfid',
      fixed: 'right',
      width: 150,
      align: 'center',
      render: (text,record) => (
        <span>
          <Button style={{marginRight: 5}} size="small" onClick={this.createRfid.bind(this, record)}>创建</Button>
          <Button size="small" onClick={this.catRfid.bind(this, record)}>查看</Button>
        </span>
      ),
    },
    {
      title: '操作',
      key: 'edit',
      fixed: 'right',
      width: 250,
      align: 'center',
      render: (text,record) => (
        <span>
          <Button type="primary" style={{marginRight: 5}} size="small" onClick={() => this.handleEdit(record)}>编辑</Button>
          <Popconfirm title="确定进行此操作?" onConfirm={() => (this.setTb(record))} okText="Yes" cancelText="No">
            <Button type="dashed"  size="small" style={{marginRight: 5}}>同步库存</Button>
          </Popconfirm>
          <Popconfirm title="确定进行此操作?" onConfirm={() => (this.setHot(record))} okText="Yes" cancelText="No">
            <Button size="small">设置热搜</Button>
          </Popconfirm>
        </span>
      ),
    },
    {
      title: '商品详情',
      key: 'cat',
      fixed: 'right',
      width: 100,
      align: 'center',
      render: (text, record) => (
        <Button type="dashed" size="small" onClick={() => this.catDetail(record)}><Icon type="search"/></Button>
      ),
    },
  ];

  gotCategoryList () {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/category/CategoryList.do', {
      adminId: adminId
    }).then((result) => {
      if (result.status === 1) {
        this.setState({
          categoryList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="商品标识码">
              {getFieldDecorator('invcode', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="商品名称">
              {getFieldDecorator('productName', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <span style={{ marginLeft: 8, color: '#1890ff'}} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </span>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }
  renderAdvancedForm() {
    const { getFieldDecorator } = this.props.form;
    const { categoryList } = this.state;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="商品标识码">
              {getFieldDecorator('invcode', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="商品名称">
              {getFieldDecorator('productName', {
                initialValue: ''
              })(<Input placeholder = "请输入" allowClear/>)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="商品状态">
              {getFieldDecorator('status', {
                initialValue: ''
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value="">全部</Option>
                  <Option value={1}>上架</Option>
                  <Option value={0}>下架</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="商品分类" style={{marginLeft: '14px'}}>
              {getFieldDecorator('categoryId', {
                initialValue: 0
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    categoryList.map((item, index) => (
                      <Option value={item.id} key={index.toString()}>{item.name}</Option>
                    ))
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="商品类型">
              {getFieldDecorator('type', {
                initialValue: 0
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={0}>全部</Option>
                  <Option value={1}>新品</Option>
                  <Option value={2}>促销</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', margin: "20px 0" }}>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              重置
            </Button>
            <span style={{ marginLeft: 8, color: '#1890ff'}} onClick={this.toggleForm}>
              收起 <Icon type="up"/>
            </span>
          </div>
        </div>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  toggleForm = () => {
    this.setState((prevState, props) => ({
      expandForm: !prevState.expandForm
    }))
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { searchform, page, limit } = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit
    }
    searchform.status && (params.status = searchform.status)
    searchform.type && (params.type = searchform.type)
    searchform.invcode && (params.invcode = searchform.invcode)
    searchform.categoryId && (params.categoryId = searchform.categoryId)
    searchform.productName && (params.productName = searchform.productName)
    this.setState({isload: true})
    http.post('/product/SelectProPage.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  setHot = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/recommend/InsertRecommend.do', {
      adminId: adminId,
      proId: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  setTb = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/product/RefFastStack.do', {
      adminId: adminId,
      proId: rows.id,
      articlenumber: rows.articlenumber,
      stoid: rows.stoid
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
    this.setState({
      editVisible: true,
      editData: rows
    });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const {adminId} = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/store/update.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  createRfid(rows) {
    this.setState({
      modalVisible: true,
      rowData: rows
    });
  }
  handleModalVisible = () => {
    this.setState({
      modalVisible: false
    });
  }
  handleAdd = (fields) => {
    const { rowData } = this.state;
    const { adminId } = this.props.appStore.loginUser;
    http.post('/rfid/InsertRFID.do', {
      adminId: adminId,
      proId: rowData.id,
      number: fields.number
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.handleModalVisible();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  };
  catRfid(rows) {
    this.setState({
      catTableVisible: true,
      catData: rows
    })
  }
  handleCatTableVisible = () => {
    this.setState({
      catTableVisible: false,
      catData: []
    })
  }
  catDetail(rows) {
    this.setState({
      goodsVisible: true,
      goodsDetail: rows
    });
  }
  closeDetail = () => {
    this.setState({
      goodsVisible: false,
      goodsDetail: []
    });
  }
  updateType () {
    const { adminId } = this.props.appStore.loginUser;
    this.setState({typeload: true})
    http.post('/conmon/GetTrimCategory.do', {
      adminId: adminId
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
      this.setState({typeload: false});
    }).catch((error) => {
      this.setState({typeload: false});
      message.error(error);
    })
  }
  updateGoods () {
    const { adminId } = this.props.appStore.loginUser;
    this.setState({goodsload: true})
    http.post('/conmon/GetTrimProduct.do', {
      adminId: adminId
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
      this.setState({goodsload: false})
    }).catch((error) => {
      this.setState({goodsload: false})
      message.error(error);
    })
  }
  updateSave () {
    const { adminId } = this.props.appStore.loginUser;
    this.setState({goodsload: true})
    http.post('/product/FastStack.do', {
      adminId: adminId
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
      } else {
        message.warning(result.msg);
      }
      this.setState({goodsload: false})
    }).catch((error) => {
      this.setState({goodsload: false})
      message.error(error);
    })
  }
  componentDidMount() {
    this.gotTableData();
    this.gotCategoryList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      modalVisible,
      catTableVisible,
      catData,
      typeload,
      goodsload,
      editVisible,
      editData,
      categoryList,
      goodsVisible,
      goodsDetail
    } = this.state;
    const addMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const catTableMethods = {
      handleCatTableVisible: this.handleCatTableVisible,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    const goodsMethods = {
      handleVisible: this.closeDetail,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['商品管理', '商品列表']}/>
        <TypingCard source={this.renderForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}} id='fixed'>
          <div className="handle-card">
            <Button type="primary" loading = {goodsload} onClick={() => (this.updateSave())}>同步商品库存</Button>
            <Button type="primary" loading = {typeload} onClick={() => (this.updateType())}>同步商品类型</Button>
            <Button type="primary" loading = {goodsload} onClick={() => (this.updateGoods())}>同步商品数据</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            scroll={{x: 3000}}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
            categoryList = {categoryList}
          />
        ) : null}
        <CreateForm {...addMethods} modalVisible={modalVisible} />
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catTableMethods}
            modalVisible={catTableVisible}
            values={catData}
          />
        ) : null}
        {goodsDetail && Object.keys(goodsDetail).length ? (
          <GoodsTable
            {...goodsMethods}
            modalVisible={goodsVisible}
            values={goodsDetail}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage