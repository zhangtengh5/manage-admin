import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Select,
  Popconfirm
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'

import TypingCard from '../../common/TypingCard'
const FormItem = Form.Item;
const Option = Select.Option;

@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    searchform: {
      account: '',
      status: ''
    },
    localBtns: [{
      id: 80,
      name: '导出',
      isshow: false
    },{
      id: 79,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '手机号', dataIndex: 'account', key: 'account',width: 120},
    {title: '订单号', dataIndex: 'orderNum', key: 'orderNum', width: 250},
    {
       title: '付款状态',
       dataIndex: 'status',
       key: 'status',
       width: 90,
       render: (text, record) => (
         record.status === 1 ? '已支付' : '未支付'
       )
    },
    {title: '货道号(x轴)', dataIndex: 'stoname', key: 'stoname', width: 120},
    {title: '货道号(y轴)', dataIndex: 'stonum', key: 'stonum', width: 120},
    {title: '设备号', dataIndex: 'equipmentName', key: 'equipmentName', width: 100},
    {title: '金额', dataIndex: 'money', key: 'money', width: 100},
    {title: '创建时间', dataIndex: 'createtime', key: 'createtime'}, 
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="手机号">
              {getFieldDecorator('account', {
                initialValue: ''
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem label="支付状态">
              {getFieldDecorator('status', {
                initialValue: ""
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value="">请选择支付方式</Option>
                  <Option value={1}>已支付</Option>
                  <Option value={0}>未支付</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      account: searchform.account,
      status: searchform.status
    }
    this.setState({isload: true})
    http.post('/vending/VendingOrderLog.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  handleExport () {
    // const { storeId } = this.props.appStore.loginUser;
    //window.location.href = '/order/downloadOrder.do?storeId=' + storeId + '&_time=' + (new Date()).getTime();
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload
    } = this.state;
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['贩卖机管理', '贩卖机订单']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Popconfirm title='确定要导出么？' onConfirm={() => (this.handleExport())} okText="Yes" cancelText="No">
              <Button type="primary" icon="download" style={{display: (this.state.localBtns[0].isshow) ? 'block': 'none'}}>导出</Button>
            </Popconfirm>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage