import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Modal,
  Select
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {}
  }
  validateStoname = (rule, value, callback) => {
    const { storeId } = this.props.appStore.loginUser;
    if (value) {
      http.post('/vending/check.do', {
        stoname: value,
        storeId: storeId
      })
      .then((result) => {
          if (result.status === 1) {
            callback()
          } else {
            callback(result.msg)
          }
      }).catch((error) => {
         callback(error)
      })
    }
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue
      }
      handleSave(values);
    });
    
  }
  render () {
    const { modalVisible, handleVisible, equipmentList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeName} = this.props.appStore.loginUser;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            < FormItem label = "贩卖机通号" >
              {getFieldDecorator('stoname',{
                rules: [
                  { required: true, message: '不能为空!' },
                  { validator: this.validateStoname}
                ],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="设备编号">
              {getFieldDecorator('equipmentName',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: ''
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    equipmentList.map((item, index) => <Option value={item.name} key={index}>{item.name}</Option>)
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="商品条码">
              {getFieldDecorator('procode',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="设备地址" >
              {getFieldDecorator('address',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="设备描述" >
              {getFieldDecorator('description',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      
    }
  }
  validateStoname = (rule, value, callback) => {
    const { storeId } = this.props.appStore.loginUser;
    const { editValues} = this.props;
    if (value && value !== editValues.stoname) {
      http.post('/vending/check.do', {
        stoname: value,
        storeId: storeId
      })
      .then((result) => {
          if (result.status === 1) {
            callback()
          } else {
            callback(result.msg)
          }
      }).catch((error) => {
         callback(error)
      })
    } else {
      callback()
    }
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { id } = this.props.editValues;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        id: id
      }
      handleSave(values);
    });
  }
  componentWillMount () {
    
  }
  componentDidMount () {
   
  }
  render () {
    const { modalVisible, handleVisible, editValues, equipmentList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeName} = this.props.appStore.loginUser;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              < FormItem label = "贩卖机通号" >
                {getFieldDecorator('stoname',{
                  rules: [
                    { required: true, message: '不能为空!' },
                    { validator: this.validateStoname}
                  ],
                  initialValue: editValues.stoname
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="所属门店">
                {getFieldDecorator('storeName',{
                  rules: [{ required: true}],
                  initialValue: storeName
                })(
                  <Input placeholder = "请输入" disabled/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="设备编号">
                {getFieldDecorator('equipmentName',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.equipmentName
                })(
                  <Select placeholder="请选择" style={{ width: '200px' }}>
                    {
                      equipmentList.map((item, index) => <Option value={item.name} key={index}>{item.name}</Option>)
                    }
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="商品条码">
                {getFieldDecorator('procode',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.procode
                })(
                  <Input placeholder = "请输入" allowClear/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="设备地址" >
              {getFieldDecorator('address',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: editValues.address
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="设备描述" >
                {getFieldDecorator('description',{
                  rules: [{ required: true, message: '内容不能为空!' }],
                  initialValue: editValues.description
                })(
                  <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      equipmentName: "",
      stoName: ""
    },
    equipmentList: [],
    localBtns: [{
      id: 77,
      name: '新增',
      isshow: false
    }, {
      id: 78,
      name: '编辑',
      isshow: false
    }, {
      id: 76,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '贩卖机通号', dataIndex: 'stoname', key: 'stoname', width: 150},
    {title: '商品ID', dataIndex: 'proid', key: 'proid', width: 80},
    {title: '设备编号', dataIndex: 'equipmentName', key: 'equipmentName', width: 100},
    {title: '地址', dataIndex: 'address', key: 'address', width: 200},
    {title: '描述', dataIndex: 'description', key: 'description'},
    {title: '创建时间', dataIndex: 'createtime', key: 'createTime', width: 250},
    {
      title: '操作',
      key: 'operation',
      width: 90,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          return (<span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)}>编辑</Button>
          </span>)
        } else {
          return <Button type="danger" size="small" disabled>编辑</Button>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="贩卖机通道号">
              {getFieldDecorator('stoName', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem label="设备编号">
              {getFieldDecorator('equipmentName', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { storeId, adminId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      storeId: storeId,
      adminId: adminId,
      page: page,
      limit: limit,
      equipmentName: searchform.equipmentName,
      stoName: searchform.stoName
    }
    this.setState({isload: true})
    http.post('/vending/GetList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId} = this.props.appStore.loginUser;
    http.post('/vending/Create.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId, storeId} = this.props.appStore.loginUser;
    http.post('/vending/Updatevending.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/store/delete.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotEquipmentList = () => {
    const { storeId } = this.props.appStore.loginUser;
    http.post('/equipment/GetAllEquipmentList.do', {
      storeid: storeId
    }).then((result) => {
      if (result.status === 1) {
        this.setState({
          equipmentList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotEquipmentList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData,
      equipmentList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['贩卖机管理', '贩卖机']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            equipmentList = {equipmentList}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
            equipmentList = {equipmentList}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage