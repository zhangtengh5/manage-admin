import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  Select,
  message,
  Modal,
  Popconfirm,
  DatePicker,
  Radio
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SingleUpload from '../../common/Upload/index'
import SingleUploadScale from '../../common/Upload/uploadscale'
import SearchTable from '../../common/SearchTable/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      goodsData: [],
      topPath: '',
      linkPath: '',
      proportion: ''
    }
  }
  gotHomeUrl = (path) => {
    //console.log(path)
    this.setState({
      topPath: path
    })
  }
  gotDetailUrl = (data) => {
    //console.log(data)
    this.setState({
      linkPath: data.path,
      proportion: data.proportion
    })
  }
  gotGoods = (goods) => {
    this.setState({
      goodsData: goods
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { goodsData, topPath, linkPath, proportion} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        beginTime: fieldsValue['beginTime'].format('YYYY-MM-DD'),
        finishTime: fieldsValue['finishTime'].format('YYYY-MM-DD'),
        prodata: JSON.stringify(goodsData),
        topPath: topPath,
        linkPath: linkPath,
        proportion: proportion
      }
      let begin = new Date(Date.parse(values.beginTime));
      let finish = new Date(Date.parse(values.finishTime));
      let prodataLength = goodsData.length;
      console.log(values)
      if (begin > finish) {
        message.warning('结束时间要大于开始时间');
        return
      } else if (prodataLength <= 0) {
        message.warning('所选商品不能为空');
        return
      } else if (!topPath) {
        message.warning('首页头图不能为空');
        return
      } else if (!linkPath) {
        message.warning('详情头图不能为空');
        return
      } else {
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible, userTypeList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeId, storeName} = this.props.appStore.loginUser;
    const { topPath, linkPath, proportion} = this.state;
    const config = {
      rules: [{ required: true, message: '请选择时间!' }],
    };
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={14}>
            <FormItem label="活动类型">
              {getFieldDecorator('type',{
                rules: [{ required: true, message: '请选择活动类型!' }],
                initialValue: 2
              })(
                <RadioGroup>
                  <Radio value={2}>告知类</Radio>
                  {/* <Radio value={1} disabled>参与类</Radio>
                  <Radio value={3} disabled>广告展示</Radio> */}
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={10}>
            <FormItem label="状态">
              {getFieldDecorator('status',{
                rules: [{ required: true, message: '请选择状态!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>上架</Radio>
                  <Radio value={0}>下架</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="活动名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ""
              })(
                < Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="投放位置">
              {getFieldDecorator('usePosition',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: 1
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={1}>轮播图(上)</Option>
                  <Option value={3}>主要活动(下)</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="活动内容" >
              {getFieldDecorator('informationPath',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="首页头图" style={{paddingLeft: '10px'}} required>
              <SingleUpload callback={this.gotHomeUrl} imageUrl= {topPath}/>
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="详情头图" style={{paddingLeft: '10px'}} required>
              <SingleUploadScale callback={this.gotDetailUrl} imageUrl= {linkPath}/>
              {
                proportion && (<p>图片比例：{proportion}</p>)
              }
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="开始时间">
              {getFieldDecorator('beginTime',config)(
                <DatePicker format="YYYY-MM-DD"  placeholder="请输入"/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="结束时间">
              {getFieldDecorator('finishTime',config)(
                <DatePicker format="YYYY-MM-DD"  placeholder="请输入"/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="适用人群">
              {getFieldDecorator('userType',{
                rules: [{ required: true, message: '选择不能为空!' }],
                initialValue: 1
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    userTypeList.map((item, index) => (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    ))
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
      <div style={{height: 500}}>
      <SearchTable placeholder="请搜索" storeId={storeId} callback={this.gotGoods}/>
     </div>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false
    }
  }
  columns = [
    {
      title: 'ID', width: 80, dataIndex: 'id', key: 'id'
    },
    {
      title: 'GID', width: 100, dataIndex: 'proId', key: 'proId'
    },
    {
      title: '商品名称', dataIndex: 'proName', key: 'proName'
    },
    {
      title: '原价', width: 150, dataIndex: 'originalPrice', key: 'originalPrice'
    },
    {
      title: '现价', width: 150, dataIndex: 'price', key: 'price'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    this.setState({isload: true})
    http.post('/poster/GetPosterItem.do', {
      posterId: id
    }).then((result) => {
      if (result.status === 1) {
        this.setState({
          tableData: result.data,
          isload: false
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload } = this.state;
    const { modalVisible, handleVisible } = this.props;
    const { startTime, endTime } = this.props.values;
    return (
      <Modal
        width={1000}
        destroyOnClose
        title="数据展示"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <div>
          <Form layout="inline" style={{marginBottom: '15px'}}>
            <FormItem label="开始时间">
             <Input
                disabled
                value={startTime}
                style={{width: '200px'}}
              />
              
            </FormItem>
            <FormItem label="结束时间">
             <Input
                disabled
                value={endTime}
                style={{width: '200px'}}
              />
            </FormItem>
          </Form>
        </div>
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {false}
          scroll={{ y: 400 }}   
        ></Table>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    catVisible: false,
    modalVisible: false,
    rowData: {},
    catTableVisible: false,
    catData: {},
    searchform: {
      type: 0
    },
    userTypeList: [],
    localBtns: [{
      id: 41,
      name: '新增',
      isshow: false
    }, {
      id: 42,
      name: '修改',
      isshow: false
    }, {
      id: 44,
      name: '删除',
      isshow: false
    }, {
      id: 43,
      name: '查询',
      isshow: false
    }, {
      id: 888,
      name: '推送',
      isshow: true
    }]
  }
  localBtns = []
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {
      title: '活动类型',
      dataIndex: 'type',
      key: 'type',
      width: 100,
      align: 'center',
      render: (type, record) => {
        if (type === 1) {
          return '参与类';
        } else if (type === 2) {
          return '告知类';
        } else {
          return '广告展示';
        }
      }
    },
    {
      title: '投放位置',
      dataIndex: 'usePosition',
      key: 'usePosition',
      width: 100,
      align: 'center',
      render: (text, record) => {
        if (record.usePosition === 1) {
          return '轮播图(上)';
        } else if (record.usePosition === 2) {
          return '重要活动(中)';
        } else {
          return '主要活动(下)';
        }
      }
    },
    {
      title: '头图',
      dataIndex: 'topPath',
      key: 'topPath',
      width: 40,
      align: 'center',
      render: (topPath) => (
        <img src={topPath} style={{width: "40px", height: "22px"}} alt=""/>
      )
    },
    {title: '活动名称', dataIndex: 'name', key: 'name', width: 200},
    {
      title: '消息推送',
      dataIndex: 'ispush',
      key: 'ispush',
      width: 100,
      align: 'center',
      render: (text, record) => (
        record.ispush === 1 ? '已推送' : '未推送'
      )
    },
    {
      title: '状态',
      key: 'status',
      dataIndex: 'status',
      width: 90,
      align: 'center',
      render: (text, record) => (
        record.status === 1 ? '上架' : '下架'
      )
    },
    {title: '开始时间', dataIndex: 'startTime', key: 'startTime', width: 180},
    {title: '结束时间', dataIndex: 'endTime', key: 'endTime', width: 180},
    {title: '适用门店', dataIndex: 'storeName', key: 'storeName', width: 150},
    
    {
      title: '使用人群',
      dataIndex: 'userType',
      key: 'userType',
      width: 100,
      align: 'center',
      render: (text, record) => {
        if (record.userType === 1) {
          return '所有人'
        }
      }
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 180},
    {
      title: '查看商品',
      key: 'nums',
      width: 70,
      align: 'center',
      render: (text, record) => (
        <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        if (record.ispush === 1) {
          //已推送
          if (this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
            return (<span>
              <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                <Button type="danger" size="small">修改</Button>
              </Popconfirm>
              <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
              <Button type="danger" size="small" disabled>已推送</Button>
            </span>)
          } else if (!this.state.localBtns[1].isshow && this.state.localBtns[2].isshow) {
            return (<span>
              <Button type="danger" size="small" disabled>修改</Button>
              <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                <Button type="danger" size="small" style={{margin: "5px 8px"}}>删除</Button>
              </Popconfirm>
              <Button type="danger" size="small" disabled>已推送</Button>
            </span>)
          } else if (!this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
            return (<span>
                <Button type="danger" size="small" disabled>修改</Button>
                <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                <Button type="danger" size="small" disabled>已推送</Button>
              </span>)
          } else {
            return (<span>
                <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">修改</Button>
                </Popconfirm>
                <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                </Popconfirm>
                <Button type="danger" size="small" disabled>已推送</Button>
              </span>)
          }
        } else {
          //未推送
          if (this.state.localBtns[4].isshow) {
            if (this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
              return (<span>
                <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">修改</Button>
                </Popconfirm>
                <Button type="danger" size="small" disabled  style={{margin: "5px 8px"}}>删除</Button>
                <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">推送</Button>
                </Popconfirm>
              </span>)
            } else if (!this.state.localBtns[1].isshow && this.state.localBtns[2].isshow) {
              return (<span>
                <Button type="danger" size="small" disabled>修改</Button>
                <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                </Popconfirm>
                <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">推送</Button>
                </Popconfirm>
              </span>)
            } else if (!this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
              return (<span>
                  <Button type="danger" size="small" disabled>修改</Button>
                  <Button type="danger" size="small" disabled  style={{margin: "0 8px"}}>删除</Button>
                  <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">推送</Button>
                  </Popconfirm>
                </span>)
            } else {
              return (<span>
                  <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">修改</Button>
                  </Popconfirm>
                  <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                  </Popconfirm>
                  <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">推送</Button>
                  </Popconfirm>
                </span>)
            }
          } else {
            if (this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
              return (<span>
                <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">修改</Button>
                </Popconfirm>
                <Button type="danger" size="small" disabled  style={{margin: "5px 8px"}}>删除</Button>
                <Button type="danger" size="small" disabled>推送</Button>
              </span>)
            } else if (!this.state.localBtns[1].isshow && this.state.localBtns[2].isshow) {
              return (<span>
                <Button type="danger" size="small" disabled>修改</Button>
                <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                </Popconfirm>
                <Button type="danger" size="small" disabled>推送</Button>
              </span>)
            } else if (!this.state.localBtns[1].isshow && !this.state.localBtns[2].isshow) {
              return (<span>
                  <Button type="danger" size="small" disabled>修改</Button>
                  <Button type="danger" size="small" disabled  style={{margin: "5px 8px"}}>删除</Button>
                  <Button type="danger" size="small" disabled>推送</Button>
                </span>)
            } else {
              return (<span>
                  <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">修改</Button>
                  </Popconfirm>
                  <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" disabled style={{margin: "5px 8px"}}>删除</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>推送</Button>
                </span>)
            }
          }
          
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="商品状态">
              {getFieldDecorator('type')(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={1}>参与类</Option>
                  <Option value={2}>告知类</Option>
                  <Option value={3}>广告展示</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      type: searchform.type
    }
    this.setState({isload: true})
    http.post('/poster/GetPosterList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId } = this.props.appStore.loginUser;
    console.log(fields)
    http.post('/poster/AddPoster.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleStatus = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    let status = 1;
    if (rows.status === 1) {
      status = 0;
    } else {
      status = 1;
    }
    http.post('/poster/UpdateStatus.do', {
      adminId: adminId,
      posterId: rows.id,
      status: status,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/poster/DeletePoster.do', {
      adminId: adminId,
      posterId: rows.id,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handlePush = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/poster/posterpush.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotUserTypeList() {
    //用户类型列表
    http.post('/poster/GetUserTypeDropList.do')
      .then((result) => {
        if (result.status === 1) {
          this.setState({
            userTypeList: result.data
          })
        } else {
          message.warning(result.msg);
        }
      }).catch((error) => {
        message.error(error);
      })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount() {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotUserTypeList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      catVisible,
      catData,
      userTypeList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '促销活动']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}} id='fixed'>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            scroll={{x: 1200}}
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            userTypeList = {userTypeList}
          />
          ) : null}
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage