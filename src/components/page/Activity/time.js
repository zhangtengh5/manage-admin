import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Modal,
  Popconfirm,
  DatePicker
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SearchTable from '../../common/SearchTable/index'
const FormItem = Form.Item;

@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      goodsData: []
    }
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { goodsData } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        beginTime: fieldsValue['beginTime'].format('YYYY-MM-DD HH:mm:ss'),
        finishTime: fieldsValue['finishTime'].format('YYYY-MM-DD HH:mm:ss'),
        prodata: JSON.stringify(goodsData)
      }
      let begin = new Date(Date.parse(values.beginTime));
      let finish = new Date(Date.parse(values.finishTime));
      let prodataLength = goodsData.length;
      if (begin > finish) {
        message.warning('结束时间要大于开始时间');
        return
      } else if (prodataLength <= 0) {
        message.warning('所选商品不能为空');
        return
      } else {
        handleSave(values);
      }
    });
    
  };
  gotGoods = (goods) => {
    //console.log(goods);
    this.setState({goodsData: goods})
  }
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeId, storeName} = this.props.appStore.loginUser;
    const config = {
      rules: [{ type: 'object', required: true, message: '请选择时间!' }],
    };
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }} style={{marginBottom: '20px'}}>
          <Col span={11}>
            <FormItem label="开始时间">
              {getFieldDecorator('beginTime',config)(
                <DatePicker format="YYYY-MM-DD HH:mm:ss" showTime placeholder="请输入"/>
              )}
            </FormItem>
          </Col>
          <Col span={11}>
            <FormItem label="结束时间">
              {getFieldDecorator('finishTime',config)(
                <DatePicker format="YYYY-MM-DD HH:mm:ss" showTime placeholder="请输入"/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('type',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                < Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
     </Form>
     <div style={{height: 500}}>
      <SearchTable placeholder="请搜索" storeId={storeId} callback={this.gotGoods}/>
     </div>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false
    }
  }
  columns = [
    {
      title: 'ID', width: 80, dataIndex: 'id', key: 'id'
    },
    {
      title: 'GID', width: 100, dataIndex: 'proId', key: 'proId'
    },
    {
      title: '商品名称', dataIndex: 'proName', key: 'proName'
    },
    {
      title: '原价', width: 150, dataIndex: 'originalPrice', key: 'originalPrice'
    },
    {
      title: '现价', width: 150, dataIndex: 'price', key: 'price'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    this.setState({isload: true})
    http.post('/flashsale/GetFlashSaleItem.do', {
      flashSaleId: id
    }).then((result) => {
      if (result.status === 1) {
        this.setState({
          tableData: result.data,
          isload: false
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload } = this.state;
    const { modalVisible, handleVisible } = this.props;
    const { startTime, endTime } = this.props.values;
    return (
      <Modal
        width={1000}
        destroyOnClose
        title="数据展示"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <div>
          <Form layout="inline" style={{marginBottom: '15px'}}>
            <FormItem label="开始时间">
             <Input
                disabled
                value={startTime}
                style={{width: '200px'}}
              />
              
            </FormItem>
            <FormItem label="结束时间">
             <Input
                disabled
                value={endTime}
                style={{width: '200px'}}
              />
            </FormItem>
          </Form>
        </div>
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {false}
          scroll={{ y: 400 }}   
        ></Table>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    catVisible: false,
    modalVisible: false,
    rowData: {},
    catTableVisible: false,
    catData: {},
    localBtns: [{
      id: 47,
      name: '新增',
      isshow: false
    },{
      id: 45,
      name: '删除',
      isshow: false
    }, {
      id: 46,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '开始时间', dataIndex: 'startTime', key: 'startTime', width: 250},
    {title: '结束时间', dataIndex: 'endTime', key: 'endTime', width: 250},
    {
      title: '商品数量',
      key: 'nums',
      
      width: 200,
      align: 'center',
      render: (text, record) => (
        <Button size="small" onClick={() => this.handleCat(record)}>{record.nums}</Button>
      ),
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 250},
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          return (<span>
            <Popconfirm title="确定进行此操作?" onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small">删除</Button>
            </Popconfirm>
          </span>)
        } else {
          return <Button type="danger" size="small" disabled>删除</Button>
        }
      },
    },
    
  ];
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit } = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit
    }
    this.setState({isload: true})
    http.post('/flashsale/GetFlashSaleList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId } = this.props.appStore.loginUser;
    http.post('/flashsale/AddFlashSale.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/flashsale/RemoveFlashSale.do', {
      adminId: adminId,
      flashSaleId: rows.id,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      catVisible,
      catData
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '限时活动']}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}} id='fixed'>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
           />
          ) : null }
        
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage