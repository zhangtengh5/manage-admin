import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Modal,
  Popconfirm,
  Tree
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const { TextArea } = Input;
const { TreeNode } = Tree;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      checkedKeys: [1]
    }
  }
  onCheck = (value) => {
    console.log('onCheck', value.checked);
    this.setState({
      checkedKeys: value.checked
    });
  }
  renderTreeNodes = data => data.map((item) => {
    if (item.children) {
      return (
        <TreeNode title={item.name} key={item.id} dataRef={item}>
          {this.renderTreeNodes(item.children)}
        </TreeNode>
      );
    }
    return <TreeNode {...item} />;
  })
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { checkedKeys } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!checkedKeys.length) {
        message.warning('权限选择不能为空');
        return
      }else {
        const values = {
          ...fieldsValue,
          powerId: checkedKeys.join(',')
        }
        handleSave(values);
      }
    });
  }
  render () {
    const { modalVisible, handleVisible, cKList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { checkedKeys} = this.state;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="角色名称">
              {getFieldDecorator('roleName',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="信息备注" >
              {getFieldDecorator('roleRemark',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ''
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="权限选择" required>
              <Tree
                checkable
                onCheck={this.onCheck}
                checkedKeys={checkedKeys}
                checkStrictly = {true}
                defaultExpandAll = {true}
              >
                {this.renderTreeNodes(cKList)}
              </Tree>
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      checkedKeys: []
    }
  }
  onCheck = (value, e) => {
    console.log('onCheck', value.checked);
    this.setState({
      checkedKeys: value.checked
    });
  }
  renderTreeNodes = data => data.map((item) => {
    if (item.children) {
      return (
        <TreeNode title={item.name} key={item.id} dataRef={item}>
          {this.renderTreeNodes(item.children)}
        </TreeNode>
      );
    }
    return <TreeNode {...item} />;
  })
  okHandle = () => {
    const { form, handleSave, editValues} = this.props;
    const { checkedKeys } = this.state;
    console.log(checkedKeys)
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!checkedKeys.length) {
        message.warning('权限选择不能为空');
        return
      }else {
        const values = {
          ...fieldsValue,
          id: editValues.id,
          powerId: checkedKeys.join(',')
        }
        handleSave(values);
      }
    });
  }
  componentDidMount () {
    const { powerlist } = this.props.editValues;
    this.setState({
      checkedKeys: powerlist
    });
  }
  render () {
    const { modalVisible, handleVisible, cKList, editValues} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { checkedKeys} = this.state;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="角色名称">
                {getFieldDecorator('roleName',{
                  rules: [{ required: true, message: '名称不能为空!' }],
                  initialValue: editValues.roleName
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="信息备注" >
                {getFieldDecorator('roleRemark',{
                  rules: [{ required: true, message: '内容不能为空!' }],
                  initialValue: editValues.roleRemark
                })(
                  <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="权限选择" required>
                <Tree
                  checkable
                  onCheck={this.onCheck}
                  checkedKeys={checkedKeys}
                  checkStrictly = {true}
                  defaultExpandAll = {true}
                >
                  {this.renderTreeNodes(cKList)}
                </Tree>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      roleName: ""
    },
    cKList: [],
    treeData:[{
        name: '首页',
        icon: 'home',
        url: '/home',
        id: 1,
        children: []
      },
      {
        name: '商品管理',
        icon: 'appstore',
        url: '/goods',
        id: 2,
        children: [{
            url: '/goods/list',
            name: '商品列表',
            icon: '',
            id: 3,
            children: []
          }
        ]
      },
    ],
    localBtns: [{
      id: 85,
      name: '新增',
      isshow: false
    }, {
      id: 87,
      name: '编辑',
      isshow: false
    }, {
      id: 86,
      name: '删除',
      isshow: false
    }, {
      id: 88,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '角色', dataIndex: 'roleName', key: 'roleName', width: 250},
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 250},
    {title: '备注', dataIndex: 'roleRemark', key: 'roleRemark'}, 
    {
      title: '操作',
      key: 'operation',
      width: 150,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[2].isshow) {
          return <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
            <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small">删除</Button>
            </Popconfirm>
          </span>
        } else {
          return <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
            <Button type="danger" size="small" disabled>删除</Button>
          </span>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="角色名字">
              {getFieldDecorator('roleName', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      limit: limit,
      roleName: searchform.roleName
    }
    this.setState({isload: true})
    http.post('/role/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    http.post('/role/InsertRole.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/role/UpadteRole.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/role/DeleteRole.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotCKList = () => {
    http.post('/power/SelectAll.do', {})
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          cKList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
        console.log(localBtns)
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotCKList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData,
      cKList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={["系统管理","角色管理"]}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            cKList= {cKList}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
            cKList= {cKList}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage