import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Select,
  message,
  Modal,
  Radio,
  Popconfirm
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SingleUpload from '../../common/Upload/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      faceImg: '',
      healthImg: ''
    }
  }
  gotFaceImg = (path) => {
    //console.log(path)
    this.setState({
      faceImg: path
    })
  }
  gotHealthImg = (path) => {
    //console.log(path)
    this.setState({
      healthImg: path
    })
  }
  validatePhone = (rule, value, callback) => {
    if (!(/^1\d{10}$/.test(value))) {
      callback('手机号不合法')
      return
    }
    callback();
  }
  validateIdCard = (rule, value, callback) => {
    if (value && !(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value))) {
      callback('身份证不合法')
      return
    }
    callback();
  }
  validatehealthNum = (rule, value, callback) => {
    if (value && !(/^[0-9]+$/.test(value)) && !(/^[a-zA-Z]+$/.test(value)) && !(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]+$/.test(value))) {
      callback('字符不合法')
      return
    }
    callback();
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { faceImg, healthImg} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!faceImg) {
        message.warning('身份证照片不能为空');
        return
      } else if (!healthImg) {
        message.warning('健康证照片不能为空');
        return
      } else {
        let storeId = '';
        if (fieldsValue.storeId.length > 1) {
          storeId = fieldsValue.storeId.join(',')
        } else {
          storeId = fieldsValue.storeId[0]
        }
        const values = {
          ...fieldsValue,
          faceImg: faceImg,
          healthImg: healthImg,
          storeId: storeId
        }
        handleSave(values);
      }
    });
  }
  render () {
    const { modalVisible, handleVisible, storeList, roleList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { faceImg, healthImg} = this.state;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="姓名">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="性别">
              {getFieldDecorator('sex',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: '男'
              })(
                <RadioGroup>
                  <Radio value={'男'}>男</Radio>
                  <Radio value={'女'}>女</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="手机号">
              {getFieldDecorator('loginName',{
                rules: [
                  { required: true, message: '不能为空!' },
                  { validator: this.validatePhone}
                ],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="登录密码">
              {getFieldDecorator('password',{
                rules: [
                  { required: true, message: '不能为空!' }
                ],
                initialValue: '123456'
              })(
                <Input placeholder = "请输入" disabled/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="身份证">
              {getFieldDecorator('idCard',{
                rules: [
                  { required: true, message: '不能为空!' },
                  { validator: this.validateIdCard}
                ],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="健康证号">
              {getFieldDecorator('healthNum',{
                rules: [
                  { required: true, message: '不能为空!' },
                  { validator: this.validatehealthNum}
                ],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="选择门店">
              {getFieldDecorator('storeId',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: []
              })(
                <Select mode="multiple" placeholder="请选择" style={{ width: '200px' }}>
                  {
                    storeList.map((item, index) => <Option value={item.id} key={index}>{item.name}</Option>)
                  }
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="选择角色">
              {getFieldDecorator('roleId',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    roleList.map((item, index) => <Option value={item.id} key={index}>{item.roleName}</Option>)
                  }
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="身份证照片" style={{paddingLeft: '10px'}} required>
              <SingleUpload callback={this.gotFaceImg} imageUrl= {faceImg}/>
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="健康证照片" style={{paddingLeft: '10px'}} required>
              <SingleUpload callback={this.gotHealthImg} imageUrl= {healthImg}/>
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="备注" >
              {getFieldDecorator('remarks',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>   
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      faceImg: '',
      healthImg: '',
      storeId: []
    }
  }
  gotFaceImg = (path) => {
    //console.log(path)
    this.setState({
      faceImg: path
    })
  }
  gotHealthImg = (path) => {
    //console.log(path)
    this.setState({
      healthImg: path
    })
  }
  validatePhone = (rule, value, callback) => {
    if (value && !(/^1\d{10}$/.test(value))) {
      callback('手机号不合法')
      return
    }
    callback();
  }
  validateIdCard = (rule, value, callback) => {
    if (value && !/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value)) {
      callback('身份证不合法')
      return
    }
    callback();
  }
 
  validatehealthNum = (rule, value, callback) => {
    if (value && !(/^[0-9]+$/.test(value)) && !(/^[a-zA-Z]+$/.test(value)) && !(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]$/.test(value))) {
      callback('字符不合法')
      return
    }
    callback();
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { faceImg, healthImg} = this.state;
    const { id } = this.props.editValues;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!faceImg) {
        message.warning('身份证照片不能为空');
        return
      } else if (!healthImg) {
        message.warning('健康证照片不能为空');
        return
      } else {
        let storeId = '';
        if (fieldsValue.storeId.length > 1) {
          storeId = fieldsValue.storeId.join(',')
        } else {
          storeId = fieldsValue.storeId[0]
        }
        const values = {
          ...fieldsValue,
          id: id,
          faceImg: faceImg,
          healthImg: healthImg,
          storeId: storeId
        }
        handleSave(values);
      }
    });
  }
  componentDidMount () {
    const { storeId, faceImg, healthImg} = this.props.editValues;
    let Value = []
    if (storeId.indexOf(',') !== -1) {
      Value = storeId.split(',')
    } else {
      Value.push(storeId)
    }
    let storeValue = Value.map((item, index) => {
      return Number(item)
    })
    // console.log(storeValue)
    this.setState({
      faceImg: faceImg,
      healthImg: healthImg,
      storeId: storeValue
    })
  }
  render () {
    const { modalVisible, handleVisible,storeList,roleList, editValues} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { faceImg, healthImg, storeId} = this.state;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
         <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="姓名">
                {getFieldDecorator('name',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.name
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="性别">
                {getFieldDecorator('sex',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.sex
                })(
                  <RadioGroup>
                    <Radio value={'男'}>男</Radio>
                    <Radio value={'女'}>女</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="手机号">
                {getFieldDecorator('loginName',{
                  rules: [
                    { required: true, message: '不能为空!' },
                    { validator: this.validatePhone}
                  ],
                  initialValue: editValues.loginName
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="身份证">
                {getFieldDecorator('idCard',{
                  rules: [
                    { required: true, message: '不能为空!' },
                    { validator: this.validateIdCard}
                  ],
                  initialValue: editValues.idCard
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="健康证号">
                {getFieldDecorator('healthNum',{
                  rules: [
                    { required: true, message: '不能为空!' },
                    { validator: this.validatehealthNum}
                  ],
                  initialValue: editValues.healthNum
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="选择门店">
                {getFieldDecorator('storeId',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: storeId
                })(
                  <Select mode="multiple" placeholder="请选择" style={{ width: '200px' }}>
                    {
                      storeList.map((item, index) => <Option value={item.id} key={index}>{item.name}</Option>)
                    }
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="选择角色">
                {getFieldDecorator('roleId',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.roleId
                })(
                  <Select placeholder="请选择" style={{ width: '200px' }}>
                    {
                      roleList.map((item, index) => <Option value={item.id} key={index}>{item.roleName}</Option>)
                    }
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="身份证照片" style={{paddingLeft: '10px'}} required>
                <SingleUpload callback={this.gotFaceImg} imageUrl= {faceImg}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="健康证照片" style={{paddingLeft: '10px'}} required>
                <SingleUpload callback={this.gotHealthImg} imageUrl= {healthImg}/>
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="活动内容" >
                {getFieldDecorator('remarks',{
                  rules: [{ required: true, message: '内容不能为空!' }],
                  initialValue: editValues.remarks
                })(
                  <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>   
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      name: ""
    },
    roleList: [],
    storeList: [],
    localBtns: [{
      id: 81,
      name: '新增',
      isshow: false
    }, {
      id: 83,
      name: '编辑',
      isshow: false
    }, {
      id: 82,
      name: '删除',
      isshow: false
    }, {
      id: 84,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '登录名', dataIndex: 'loginName', key: 'loginName', width: 100},
    {title: '名字', dataIndex: 'name', key: 'name', width: 100},
    {title: '角色', dataIndex: 'roleName', key: 'roleName', width: 100},
    {title: '性别', dataIndex: 'sex', key: 'sex', width: 60},
    {title: '身份证', dataIndex: 'idCard', key: 'idCard', width: 200},
    {title: '健康证', dataIndex: 'healthNum', key: 'healthNum', width: 200},
    {title: '门店权限', dataIndex: 'roleStore', key: 'roleStore', width: 250},
    {
      title: '员工照片',
      dataIndex: 'faceImg',
      key: 'faceImg',
      width: 40,
      align: 'center',
      render: (img) => (
        <img src={img} style={{width: "40px", height: "22px"}} alt=""/>
      )
    },
    {
      title: '健康证照片',
      dataIndex: 'healthImg',
      key: 'healthImg',
      width: 100,
      align: 'center',
      render: (img) => (
        <img src={img} style={{width: "40px", height: "22px"}} alt=""/>
      )
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 200},
    {title: '备注', dataIndex: 'remarks', key: 'remarks', width: 200},
    {
      title: '操作',
      key: 'operation',
      width: 150,
      fixed: 'right',
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[2].isshow) {
          return <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
            <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small">删除</Button>
            </Popconfirm>
          </span>
        } else {
          return <span>
              <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
              <Button type="danger" size="small" disabled>删除</Button>
            </span>
        }
       
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="姓名/手机号/身份证">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId} = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      storeId: storeId,
      limit: limit,
      name: searchform.name
    }
    this.setState({isload: true})
    http.post('/admin/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    http.post('/admin/AddAdmin.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/admin/Update.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/admin/Delete.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotRoleList = () => {
    http.post('/role/SelectRole.do', {})
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          roleList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotStoreList = () => {
    http.post('/store/SelectAllStore.do', {})
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          storeList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotRoleList();
    this.gotStoreList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData,
      storeList,
      roleList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['系统管理','员工管理']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            scroll={{x: 1950}}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            storeList= {storeList}
            roleList = {roleList}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
            storeList= {storeList}
            roleList = {roleList}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage