import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  Select,
  message,
  Modal,
  Popconfirm,
  Radio,
  InputNumber
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SearchInput from '../../common/SearchInput/index'
import TypingCard from '../../common/TypingCard'
import SearchTable from '../../common/SearchTable/index'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      selectType: 'B',
      buyGoods: {},
      sendGoods: {},
      zkGoods: {},
      goodsData: []
    }
  }
  typeB () {
    const { getFieldDecorator } = this.props.form;
    const { storeId} = this.props.appStore.loginUser;
    return (
      <div>
        <div></div>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="购买商品" required>
              <SearchInput placeholder="请搜索" style={{width: "350px"}} storeId={storeId} callback={this.gotBuy}/>
            </FormItem>
            <FormItem label="购买数量">
              {getFieldDecorator('buyPronum', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '100px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="赠送商品" required>
              <SearchInput placeholder="请搜索" style={{width: "350px"}} storeId={storeId} callback={this.gotSend}/>
            </FormItem>
            <FormItem label="赠送数量">
              {getFieldDecorator('sendPronum', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '100px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </div>
    )
  }
  typeC () {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="满减额度">
              {getFieldDecorator('discountSum', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "例如满20减5，输入20"
                  allowClear 
                  style={{width: '200px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('discountMoney', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "例如满20减5，输入5"
                  allowClear 
                  style={{width: '200px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        
     </div>
    )
  }
  typeA () {
    const { getFieldDecorator } = this.props.form;
    const { storeId} = this.props.appStore.loginUser;
    return (
      <div>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="商品选择" required>
              <SearchInput placeholder="请搜索" style={{width: "580px"}} storeId={storeId} callback={this.gotZheKou}/>
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="折扣额度">
              {getFieldDecorator('discountPercent', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "折扣额度，例如9折输入9"
                  allowClear 
                  style={{width: '200px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </div>
    )
  }
  currentType () {
    const {selectType} = this.state;
    if (selectType === 'C') {
      return this.typeC();
    } else if (selectType === 'B') {
      return this.typeB();
    } else {
      return this.typeA();
    }
  }
  handleChange = (value) => {
    this.setState({
      selectType: value
    })
  }
  gotZheKou = (value) => {
    this.setState({
      zkGoods: value
    })
  }
  gotBuy = (value) => {
    this.setState({
      buyGoods: value
    })
  }
  gotSend = (value) => {
    this.setState({
      sendGoods: value
    })
  }
  gotGoods = (value) => {
    this.setState({
      goodsData: value
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const {selectType, zkGoods, buyGoods, sendGoods, goodsData} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (selectType === 'A') {
        if (!zkGoods) {
          message.warning('请选择商品');
          return
        } else {
          const values = {
            ...fieldsValue,
            discountProId: zkGoods.key,

          }
          handleSave(values);
        }
      } else if (selectType === 'C') {
        if (goodsData.length <= 0) {
          message.warning('请选择商品');
          return
        } else {
          let proId = '';
          goodsData.map((item, index) => (
            proId += item.proId + ','
          ))
          const values = {
            ...fieldsValue,
            proId: proId.substring(0, (proId.length - 1))
          }
          handleSave(values);
        }
      } else {
        if (!buyGoods || !sendGoods) {
          message.warning('请选择商品');
          return
        } else {
          const values = {
            ...fieldsValue,
            buyProid: buyGoods.key,
            sendProid: sendGoods.key
          }
          handleSave(values);
        }
      }
    });
    
  }
  render () {
    const {selectType} = this.state;
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeName, storeId} = this.props.appStore.loginUser;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="活动类型">
              {getFieldDecorator('saleType',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: "B"
              })(
                <Select placeholder="请选择" style={{ width: '200px' }} onChange={this.handleChange}>
                  <Option value="A">折扣类</Option>
                  <Option value="C">满减类</Option>
                  <Option value="B">买赠类</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="活动名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ""
              })(
                < Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
           <Col span={12}>
            <FormItem label="状态">
              {getFieldDecorator('status',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>上架</Radio>
                  <Radio value={0}>下架</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        {
          this.currentType()
        }
      </Form>
      {
        selectType === 'C' ? (
          <div style={{height: 500}}>
            <SearchTable placeholder="请搜索" storeId={storeId} callback={this.gotGoods}/>
          </div>
        ) : null
      }
    </Modal>
    )
  }
}

@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'GID', width: 100, dataIndex: 'proid', key: 'proid'
    },
    {
      title: '商品名称', dataIndex: 'proName', key: 'proName'
    },
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {limit, page} = this.state;
    this.setState({isload: true})
    http.post('/SalesPromotion/SalesPromotionItems.do', {
      id: id,
      limit: limit,
      page: page
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="满减类商品"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey = "proid"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    catVisible: false,
    rowData: {},
    catTableVisible: false,
    catData: {},
    searchform: {
      type: "",
      name: ""
    },
    localBtns: [{
      id: 59,
      name: '新增',
      isshow: false
    }, {
      id: 60,
      name: '删除',
      isshow: false
    }, {
      id: 58,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {
      title: '活动类型',
      dataIndex: 'saleType',
      key: 'saleType',
      width: 80,
      align: 'center',
      render: (text, record) => {
        if (text === "A") {
          return '折扣类';
        } else if (text === "B") {
          return '满赠类';
        } else {
          return '满减类';
        }
      }
    },
    {title: '活动名称', dataIndex: 'name', key: 'name', width: 200},
    {
      title: '状态',
      key: 'status',
      dataIndex: 'status',
      width: 80,
      align: 'center',
      render: (text, record) => (
        record.status === 1 ? '上架' : '下架'
      )
    }, {
      title: '创建时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width: 120
    },
    {
      title: '折扣商品/折扣数',
      dataIndex: 'discountProName',
      key: 'discountProName',
      width: 200,
      align: 'center',
      render: (text, record) => {
        if (record.saleType === 'A') {
          return record.discountProName + '/折扣数(' + record.discountPercent + ')'
        }
        return ''
      }
    },
    {
      title: '满/减',
      dataIndex: 'discountSum',
      key: 'discountSum',
      width: 150,
      align: 'center',
      render: (text, record) => {
        if (record.saleType === 'C') {
          return '满' + record.discountSum + '/减' + record.discountMoney
        }
        return ''
      }
    },
    {
      title: '买类商品/数量',
      dataIndex: 'buyProid',
      key: 'buyProid',
      width: 200,
      align: 'center',
      render: (text, record) => {
        if (record.saleType === 'B') {
          return record.buyProName + '/数量(' + record.buyPronum + ')'
        }
        return ''
      }
    },
    {
      title: '赠类商品/数量',
      dataIndex: 'sendProid',
      key: 'sendProid',
      width: 200,
      align: 'center',
      render: (text, record) => {
        if (record.saleType === 'B') {
          return record.sendProName + '/数量(' + record.sendPronum + ')'
        }
        return ''
      }
    }, 
    {
      title: '满减商品',
      key: 'cat',
      width: 80,
      align: 'center',
      render: (text, record) => {
        if (record.saleType === 'C') {
          return <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
        }
        return <Button size="small" disabled><Icon type="search"/></Button>
      }
    },
    {
      title: '操作',
      key: 'operation',
      width: 80,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          return <span>
            <Popconfirm title={record.status === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small">修改</Button>
            </Popconfirm>
          </span>
        } else {
          return <Button type="danger" size="small" disabled>修改</Button>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="活动名称">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem label="活动类型">
              {getFieldDecorator('type')(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value="A">折扣类</Option>
                  <Option value="B">买赠类</Option>
                  <Option value="C">满减类</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      type: searchform.type,
      name: searchform.name
    }
    this.setState({isload: true})
    http.post('/SalesPromotion/SalesPromotionList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId } = this.props.appStore.loginUser;
    http.post('/SalesPromotion/AddSalesPromotion.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
 
  handleStatus = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    let status = 1;
    if (rows.status === 1) {
      status = 0;
    } else {
      status = 1;
    }
    http.post('/SalesPromotion/UpdateStatus.do', {
      adminId: adminId,
      id: rows.id,
      status: status,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      catVisible,
      catData
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '结算柜促销']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}} id='fixed'>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            scroll={{x: 1200}}
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
          />
          ) : null}
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage