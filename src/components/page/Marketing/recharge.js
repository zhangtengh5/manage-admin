import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Modal,
  DatePicker,
  Radio,
  InputNumber
} from 'antd'
import moment from 'moment';
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {},
    values: {},
  }
  constructor(props) {
    super(props)
    this.state = {
      
    }
  }
  okHandle = () => {
    const { form, handleSave, values} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const params = {
        ...fieldsValue,
        begintime: fieldsValue['begintime'].format('YYYY-MM-DD'),
        finishtime: fieldsValue['finishtime'].format('YYYY-MM-DD'),
        id: values.id
      }
      let begin = new Date(Date.parse(values.begintime));
      let finish = new Date(Date.parse(values.finishtime));
      if (begin > finish) {
        message.warning('结束时间要大于开始时间');
        return
      } else if (params.sendmoney1 > params.showmoney1) {
        message.warning('赠送金额1不能大于展示金额1');
        return
      } else if (params.sendmoney2 > params.showmoney2) {
        message.warning('赠送金额2不能大于展示金额2');
        return
      } else if (params.sendmoney3 > params.showmoney3) {
        message.warning('赠送金额3不能大于展示金额3');
        return
      } else if (params.sendmoney4 > params.showmoney4) {
        message.warning('赠送金额4不能大于展示金额4');
        return
      } else if (params.sendmoney5 > params.showmoney5) {
        message.warning('赠送金额5不能大于展示金额5');
        return
      } else if (params.sendmoney6 > params.showmoney6) {
        message.warning('赠送金额6不能大于展示金额6');
        return
      }
      handleSave(params);
    });
    
  }
  componentDidMount() {
    // console.log(this.props)
  }
  render () {
    
    const { getFieldDecorator } = this.props.form;
    const { modalVisible, handleVisible, values} = this.props;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="编辑"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额1">
              {getFieldDecorator('showmoney1', {
                initialValue: values.showmoney1,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额1">
              {getFieldDecorator('sendmoney1', {
                initialValue: values.sendmoney1,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额2">
              {getFieldDecorator('showmoney2', {
                initialValue: values.showmoney2,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额2">
              {getFieldDecorator('sendmoney2', {
                initialValue: values.sendmoney2,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额3">
              {getFieldDecorator('showmoney3', {
                initialValue: values.showmoney3,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额3">
              {getFieldDecorator('sendmoney3', {
                initialValue: values.sendmoney3,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额4">
              {getFieldDecorator('showmoney4', {
                initialValue: values.showmoney4,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额4">
              {getFieldDecorator('sendmoney4', {
                initialValue: values.sendmoney4,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额5">
              {getFieldDecorator('showmoney5', {
                initialValue: values.showmoney5,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额5">
              {getFieldDecorator('sendmoney5', {
                initialValue: values.sendmoney5,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="展示金额6">
              {getFieldDecorator('showmoney6', {
                initialValue: values.showmoney6,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="赠送金额6">
              {getFieldDecorator('sendmoney6', {
                initialValue: values.sendmoney6,
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={0}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '240px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="开始时间">
              {getFieldDecorator('begintime',{
                rules: [{ required: true, message: '请选择时间!' }],
                initialValue: moment(values.begintime, 'YYYY-MM-DD')
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择" style={{width: '240px'}}/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="结束时间">
              {getFieldDecorator('finishtime',{
                rules: [{ required: true, message: '请选择时间!' }],
                initialValue: moment(values.finishtime, 'YYYY-MM-DD')
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择" style={{width: '240px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="充值次数">
              {getFieldDecorator('isfirst',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: values.isfirst
              })(
                <RadioGroup>
                  <Radio value={0}>单次充值</Radio>
                  <Radio value={1}>多次充值</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      
    }
  }
  okHandle = () => {
    const { handleSave } = this.props;
    handleSave();
  }
  componentDidMount () {
    
  }
  render () {
    const { modalVisible, handleVisible, values} = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="查看"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额1">
                {getFieldDecorator('showmoney1', {
                  initialValue: values.showmoney1,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    disabled
                    placeholder = "请输入"
                    allowClear
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额1">
                {getFieldDecorator('sendmoney1', {
                  initialValue: values.sendmoney1,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额2">
                {getFieldDecorator('showmoney2', {
                  initialValue: values.showmoney2,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额2">
                {getFieldDecorator('sendmoney2', {
                  initialValue: values.sendmoney2,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额3">
                {getFieldDecorator('showmoney3', {
                  initialValue: values.showmoney3,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额3">
                {getFieldDecorator('sendmoney3', {
                  initialValue: values.sendmoney3,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额4">
                {getFieldDecorator('showmoney4', {
                  initialValue: values.showmoney4,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额4">
                {getFieldDecorator('sendmoney4', {
                  initialValue: values.sendmoney4,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额5">
                {getFieldDecorator('showmoney5', {
                  initialValue: values.showmoney5,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额5">
                {getFieldDecorator('sendmoney5', {
                  initialValue: values.sendmoney5,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    placeholder = "请输入"
                    allowClear
                    disabled
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="展示金额6">
                {getFieldDecorator('showmoney6', {
                  initialValue: values.showmoney6,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={1}
                    disabled
                    placeholder = "请输入"
                    allowClear 
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="赠送金额6">
                {getFieldDecorator('sendmoney6', {
                  initialValue: values.sendmoney6,
                  rules: [{ required: true, message: '请输入正整数!'}],
                })(<InputNumber 
                    min={0}
                    disabled
                    placeholder = "请输入"
                    allowClear 
                    style={{width: '240px'}}
                    formatter = {
                      (value) => {
                        if (value && !isNaN(value)) {
                          return parseInt(value)
                        } else {
                          return ''
                        }
                      }
                    }
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="开始时间">
                {getFieldDecorator('starttime',{
                  rules: [{ required: true, message: '请请输入!' }],
                  initialValue: values.starttime
                })(
                  <Input placeholder = "请输入" disabled style={{width: '240px'}}/>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="结束时间">
                {getFieldDecorator('endtime',{
                  rules: [{ required: true, message: '请请输入!' }],
                  initialValue: values.endtime
                })(
                  <Input placeholder = "请输入" disabled style={{width: '240px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="充值次数">
                {getFieldDecorator('isfirst',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: values.isfirst
                })(
                  <RadioGroup>
                    <Radio value={0} disabled>单次充值</Radio>
                    <Radio value={1} disabled>多次充值</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}




@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      begintime: '',
      finishtime: ''
    },
    localBtns: [{
      id: 100,
      name: '编辑',
      isshow: false
    },{
        id: 102,
        name: '导出',
        isshow: false
    }, {
        id: 101,
        name: '查看',
        isshow: false
    }],
    addData: {}
  }
  columns = [
    {title: '操作人', dataIndex: 'createname', key: 'createname', width: 100},
    {title: '操作时间', dataIndex: 'createtime', key: 'createtime', width: 250},
    {title: '充值次数', dataIndex: 'firstname', key: 'firstname', width: 100},
    {title: '开始时间', dataIndex: 'starttime', key: 'starttime', width: 200},
    {title: '结束时间', dataIndex: 'endtime', key: 'endtime', width: 200},
    {
      title: '展示/赠送金额',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        return (
          <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)}>查看</Button>
          </span>
        )
      },
    }
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="开始时间">
              {getFieldDecorator('begintime',{
                
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
            <FormItem label="结束时间">
              {getFieldDecorator('finishtime',{
               
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  gotMoneyList () {
    http.post('/rechargelog/info.do')
      .then((result) => {
        if (result.status === 1) {
          this.setState({
            addData: result.data
          })
        } else {
          message.warning(result.msg);
        }
      }).catch((error) => {
        message.error(error);
      })
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let params = {}
      if (fieldsValue['begintime'] && !fieldsValue['finishtime']) {
        params = {
          ...fieldsValue,
          begintime: fieldsValue['begintime'].format('YYYY-MM-DD')
        }
      } else if (!fieldsValue['begintime'] && fieldsValue['finishtime']) {
        params = {
          ...fieldsValue,
          finishtime: fieldsValue['finishtime'].format('YYYY-MM-DD')
        }
      } else if (!fieldsValue['begintime'] && !fieldsValue['finishtime']) {
        params = {
          begintime: '',
          finishtime: ''
        };
      } else {
        params = {
          ...fieldsValue,
          begintime: fieldsValue['begintime'].format('YYYY-MM-DD'),
          finishtime: fieldsValue['finishtime'].format('YYYY-MM-DD')
        }
      }
      
      console.log(params)
      this.setState({
        searchform: params
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { page, limit, searchform} = this.state;
    let params = {
      page: page,
      limit: limit,
      begintime: searchform.begintime,
      finishtime: searchform.finishtime
    }
    this.setState({isload: true})
    http.post('/rechargelog/logs.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/rechargelog/updateinfo.do', {
      adminid: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
        this.gotMoneyList();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
    this.setState({
      editVisible: true,
      editData: rows
    });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: {}
    });
  }
  saveEdit = (fields) => {
    this.setState({
      editVisible: false,
      editData: {}
    });
  }
  handleExport() {
    const {searchform } = this.state;
    window.location.href = "/rechargelog/downloadlogs.do?begintime=" + searchform.begintime + '&finishtime=' + searchform.finishtime
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotMoneyList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      addData,
      editVisible,
      editData
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '充值活动']}/>
        <Card bordered={true} title='当前充值活动' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>编辑</Button>
          </div>
          <div className="table-rows">
            <div className="table-row table-row1">
              <h2>序号</h2>
              <ul>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>6</li>
              </ul>
            </div>
            <div className="table-row table-row2">
              <h2>展示金额</h2>
              <ul>
                <li>{addData.showmoney1}</li>
                <li>{addData.showmoney2}</li>
                <li>{addData.showmoney3}</li>
                <li>{addData.showmoney4}</li>
                <li>{addData.showmoney5}</li>
                <li>{addData.showmoney6}</li>
              </ul>
            </div>
            <div className ="table-row table-row3" >
              <h2>赠送金额</h2>
              <ul>
                <li>{addData.sendmoney1}</li>
                <li>{addData.sendmoney2}</li>
                <li>{addData.sendmoney3}</li>
                <li>{addData.sendmoney4}</li>
                <li>{addData.sendmoney5}</li>
                <li>{addData.sendmoney6}</li>
              </ul>
            </div>
            <div className="table-row table-row4">
              <h2>充值次数</h2>
              <div>{addData.isfirst ? '多次充值' : '单次充值'}</div>
            </div>
            <div className="table-row table-row5">
              <h2>开始时间/结束时间</h2>
              <div>
                <span>{addData.begintime}-{addData.finishtime}</span>
              </div>
            </div>
          </div>
        </Card>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='操作记录' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleExport())}>导出</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible && addData ? (
          < AddForm
            {...addMethods}
            modalVisible={addVisible}
            values={addData}
          />
        ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            values={editData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage