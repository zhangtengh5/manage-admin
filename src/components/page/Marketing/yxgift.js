import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  Select,
  message,
  Modal,
  DatePicker,
  Radio,
  Popconfirm
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SearchCoupon from '../../common/SearchCouponAll/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      goodsData: []
    }
  }
  gotGoods = (goods) => {
    //console.log(goods);
    this.setState({
      goodsData: goods
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { goodsData} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (goodsData.length <= 0) {
        message.warning('所选优惠券不能为空');
        return
      } else {
        let couponIds = '';
        goodsData.map((item, index) => (
          couponIds += item.id + ','
        ))
        const values = {
          ...fieldsValue,
          couponIds: couponIds.substring(0, (couponIds.length - 1)),
          finishTime: fieldsValue['finishTime'].format('YYYY-MM-DD')
        }
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeName, storeId} = this.props.appStore.loginUser;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="礼包类型">
              {getFieldDecorator('type',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={1}>节日礼包</Option>
                  <Option value={2}>品牌商品礼包</Option>
                  <Option value={3}>店庆礼包</Option>
                  <Option value={4}>促销礼包</Option>
                  <Option value={5}>季节礼包</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="礼包名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="状态">
              {getFieldDecorator('status',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>上架</Radio>
                  <Radio value={0}>下架</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="结束时间">
              {getFieldDecorator('finishTime',{
                rules: [{ required: true, message: '请选择时间!' }]
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="内容">
              {getFieldDecorator('description',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="备注">
              {getFieldDecorator('remark',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
      <div style={{height: 500}}>
        <SearchCoupon placeholder="请搜索" storeId={storeId} callback={this.gotGoods}/>
      </div>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      
    }
  }
  okHandle = () => {
    const { form, handleSave, values} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const params = {
        id: values.id,
        ...fieldsValue
      }
      handleSave(params);
    });
  }
  componentDidMount () {
    
  }
  render () {
    const { modalVisible, handleVisible, values} = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="状态">
                {getFieldDecorator('status',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: values.status
                })(
                  <RadioGroup>
                    <Radio value={1}>上架</Radio>
                    <Radio value={0}>下架</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="备注">
                {getFieldDecorator('remark',{
                  rules: [{ required: true, message: '内容不能为空!' }],
                  initialValue: values.remark
                })(
                  <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '300px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'ID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: '优惠券ID', width: 100, dataIndex: 'couponId', key: 'couponId'
    },
    {
      title: '优惠券名称', dataIndex: 'couponName', key: 'couponName'
    },
    {
      title: '优惠券类型', dataIndex: 'coupontype', key: 'coupontype'
    },
    {
      title: '优惠券描述', dataIndex: 'description', key: 'description'
    },
    {
      title: '领取方式',
      dataIndex: 'effective',
      key: 'effective',
      width: 120,
      align: 'center',
      render: (text, record) => (
        record.effective === 1 ? '手动领取' : '系统发放'
      )
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {limit, page} = this.state;
    this.setState({isload: true})
    http.post('/promotionalGift/SelectItemById.do', {
      id: id,
      limit: limit,
      page: page
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="优惠券列表"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}



@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    catVisible: false,
    catData: {},
    searchform: {
      type: '',
      status: '',
      name: ''
    },
    localBtns: [{
      id: 56,
      name: '新增',
      isshow: false
    }, {
      id: 57,
      name: '编辑',
      isshow: false
    }, {
      id: 55,
      name: '查询',
      isshow: false
    }, {
      id: 97,
      name: '推送',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '所属店铺', dataIndex: 'storeName', key: 'storeName', width: 150},
    {
      title: '礼包类型',
      key: 'type',
      width: 100,
      align: 'center',
      render: (text, record) => {
        if (record.type === 1) {
          return '节日礼包'
        } else if (record.type === 2) {
          return '品牌商品礼包'
        } else if (record.type === 3) {
          return '店庆礼包'
        } else if (record.type === 4) {
          return '促销礼包'
        } else if (record.type === 5) {
          return '促销礼包'
        }
      }
    },
    {title: '礼包名称', dataIndex: 'name', key: 'name', width: 200},
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 120,
      align: 'center',
      render: (text, record) => (
        record.status === 1 ? '上架' : '下架'
      )
    },
    {
      title: '消息推送',
      dataIndex: 'ispush',
      key: 'ispush',
      width: 100,
      align: 'center',
      render: (text, record) => (
        record.ispush === 1 ? '已推送' : '未推送'
      )
    },
    {title: '礼包内容', dataIndex: 'description', key: 'description', width: 150},
    {title: '失效时间', dataIndex: 'endTime', key: 'endTime', width: 200},
    {title: '备注', dataIndex: 'remark', key: 'remark'},
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 200},
    {
      title: '优惠券',
      key: 'cat',
      width: 80,
      align: 'center',
      render: (text, record) => (
        <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      width: 220,
      align: 'center',
      render: (text,record) => {
        if (!this.state.localBtns[3].isshow) {
          if (record.ispush === 1) {
             return (
              <span>
                <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '5px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
                <Button type="danger" size="small" disabled>已推送</Button>
              </span>
            )
          } else {
            return (
              <span>
                <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '5px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
                <Button type="danger" size="small" disabled>已推送</Button>
              </span>
            )
          }
        } else {
          if (record.ispush === 1) {
             return (
              <span>
                <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '5px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
                <Button type="danger" size="small" disabled>已推送</Button>
              </span>
            )
          } else {
            return (
              <span>
                <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginRight: '5px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
                <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                  <Button type="danger" size="small">推送</Button>
                </Popconfirm>
              </span>
            )
          }
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="礼包名称">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem label="礼包类型">
              {getFieldDecorator('type')(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={1}>节日礼包</Option>
                  <Option value={2}>品牌商品礼包</Option>
                  <Option value={3}>店庆礼包</Option>
                  <Option value={4}>促销礼包</Option>
                  <Option value={5}>季节礼包</Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="礼包状态">
              {getFieldDecorator('status')(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  <Option value={1}>上架</Option>
                  <Option value={0}>下架</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId, storeId} = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      limit: limit,
      type: searchform.type,
      status: searchform.status,
      name: searchform.name,
      storeId: storeId
    }
    this.setState({isload: true})
    http.post('/promotionalGift/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId } = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/promotionalGift/InsertGifts.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/promotionalGift/updateGiftsStatus.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handlePush = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/promotionalGift/giftpush.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData,
      catVisible,
      catData
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '营销礼包']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            values={editData}
          />
        ) : null}
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage