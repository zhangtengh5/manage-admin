import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  Select,
  message,
  Modal,
  Popconfirm,
  DatePicker,
  Radio,
  InputNumber
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SearchInput from '../../common/SearchInput/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;

@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      selectType: 'A',
      radioValue: 1,
      buyGoods: {},
      sendGoods: {}
    }
  }
  typeB () {
    const { getFieldDecorator } = this.props.form;
    const { storeId} = this.props.appStore.loginUser;
    return (
      <div>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={15}>
            <FormItem label="购买商品" required>
              <SearchInput placeholder="请搜索" style={{width: "350px"}} storeId={storeId} callback={this.gotBuy}/>
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label="购买数量">
              {getFieldDecorator('proNum', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '100px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={15}>
            <FormItem label="赠送商品" required>
              <SearchInput placeholder="请搜索" style={{width: "350px"}} storeId={storeId} callback={this.gotSend}/>
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label="赠送数量">
              {getFieldDecorator('complimentaryPronum', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "请输入"
                  allowClear 
                  style={{width: '100px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </div>
    )
  }
  typeE () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="现金额度">
            {getFieldDecorator('discountCash', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "请输入"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="是否叠加">
            {getFieldDecorator('superposition',{
              rules: [{ required: true, message: '请选择!' }],
              initialValue: 0
            })(
              <RadioGroup>
                <Radio value={1}>是</Radio>
                <Radio value={0}>否</Radio>
              </RadioGroup>
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  typeC () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="满减额度">
            {getFieldDecorator('discountSum', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "例如满20减5，输入20"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem>
            {getFieldDecorator('discountMoney', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "例如满20减5，输入5"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  typeA () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="折扣额度">
            {getFieldDecorator('discountPercent', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                max={9}
                placeholder = "折扣额度，例如9折输入9"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  amount () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="优惠券数量">
            {getFieldDecorator('couponIndex', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "请输入"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  currentType () {
    const {selectType} = this.state;
    if (selectType === 'C') {
      return this.typeC();
    } else if (selectType === 'E') {
      return this.typeE();
    } else if (selectType === 'B') {
      return this.typeB();
    } else {
      return this.typeA();
    }
  }
  handleChange = (value) => {
    this.setState({
      selectType: value
    })
  }
  changeRadio = (value) => {
    this.setState({
      radioValue: value.target.value
    })
  }
  gotBuy = (value) => {
    this.setState({
      buyGoods: value
    })
  }
  gotSend = (value) => {
    this.setState({
      sendGoods: value
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const {buyGoods, sendGoods, selectType} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      let values = {
        ...fieldsValue,
        startTime: fieldsValue['startTime'].format('YYYY-MM-DD'),
        endTime: fieldsValue['endTime'].format('YYYY-MM-DD')
      }
      let begin = new Date(Date.parse(values.startTime));
      let finish = new Date(Date.parse(values.endTime));
      if (begin > finish) {
        message.warning('结束时间要大于开始时间');
        return
      } else {
        if (selectType === 'B') {
          if (!buyGoods || !sendGoods) {
            message.warning('买/赠商品不能为空');
            return
          } else {
            let prodata = [];
            prodata[0] = {proId: buyGoods.key}
            const valueB = {
              ...values,
              complimentaryProid: sendGoods.key,
              prodata: JSON.stringify(prodata)
            }
            handleSave(valueB);
          }
        } else {
          handleSave(values);
        }
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible, userTypeList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { storeName} = this.props.appStore.loginUser;
    const {radioValue} = this.state;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="领取方式">
              {getFieldDecorator('effective',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                < RadioGroup onChange={this.changeRadio}>
                  <Radio value={1}>手动领取</Radio>
                  <Radio value={0}>系统发放</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="优惠券类型">
              {getFieldDecorator('type',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: "A"
              })(
                <Select placeholder="请选择" style={{ width: '200px' }} onChange={this.handleChange}>
                  <Option value="A">折扣类</Option>
                  <Option value="C">满减类</Option>
                  <Option value="E">代金类</Option>
                  <Option value="B">买赠类</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="优惠券名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ""
              })(
                < Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime',{
                rules: [{ required: true, message: '请选择时间!' }]
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime',{
                rules: [{ required: true, message: '请选择时间!' }]
              })(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="优惠券描述">
              {getFieldDecorator('description',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "优惠券描述不能超过25字" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="适用人群">
              {getFieldDecorator('userType',{
                rules: [{ required: true, message: '选择不能为空!' }],
                initialValue: 1
              })(
                 <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    userTypeList.map((item, index) => (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    ))
                  }
                  
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="状态">
              {getFieldDecorator('isshow',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>上架</Radio>
                  <Radio value={0}>下架</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        {
          radioValue === 1 ? (this.amount()) : null
        }
        {
          this.currentType()
        }
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class AddFormTY extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      selectType: 'A'
    }
  }
  typeE () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="现金额度">
            {getFieldDecorator('discountCash', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "请输入"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="是否叠加">
            {getFieldDecorator('superposition',{
              rules: [{ required: true, message: '请选择!' }],
              initialValue: 0
            })(
              <RadioGroup>
                <Radio value={1}>是</Radio>
                <Radio value={0}>否</Radio>
              </RadioGroup>
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  typeC () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="满减额度">
            {getFieldDecorator('discountSum', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "例如满20减5，输入20"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem>
            {getFieldDecorator('discountMoney', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                placeholder = "例如满20减5，输入5"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  typeA () {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
        <Col span={12}>
          <FormItem label="折扣额度">
            {getFieldDecorator('discountPercent', {
              initialValue: '',
              rules: [{ required: true, message: '请输入正整数!'}],
            })(<InputNumber 
                min={1}
                max={9}
                placeholder = "折扣额度，例如9折输入9"
                allowClear 
                style={{width: '200px'}}
                formatter = {
                  (value) => {
                    if (value && !isNaN(value)) {
                      return parseInt(value)
                    } else {
                      return ''
                    }
                  }
                }
              />
            )}
          </FormItem>
        </Col>
      </Row>
    )
  }
  currentType () {
    const {selectType} = this.state;
    if (selectType === 'C') {
      return this.typeC();
    } else if (selectType === 'E') {
      return this.typeE();
    } else {
      return this.typeA();
    }
  }
  handleChange = (value) => {
    console.log(value)
    this.setState({
      selectType: value
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        startTime: fieldsValue['startTime'].format('YYYY-MM-DD'),
        endTime: fieldsValue['endTime'].format('YYYY-MM-DD')
      }
      console.log(values)
      let begin = new Date(Date.parse(values.startTime));
      let finish = new Date(Date.parse(values.endTime));
      if (begin >= finish) {
        message.warning('结束时间要大于开始时间');
        return
      } else {
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible, userTypeList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const config = {
      rules: [{ required: true, message: '请选择时间!' }],
    };
    return (
      <Modal
      destroyOnClose
      width={800}
      title = "通用店铺优惠券新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="领取方式">
              {getFieldDecorator('effective',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 0
              })(
                <RadioGroup>
                  <Radio value={0} disabled>系统发放</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: '通用门店'
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="优惠券类型">
              {getFieldDecorator('type',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: "A"
              })(
                <Select placeholder="请选择" style={{ width: '200px' }} onChange={this.handleChange}>
                  <Option value="A">折扣类</Option>
                  <Option value="C">满减类</Option>
                  <Option value="E">代金类</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="优惠券名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ""
              })(
                < Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="开始时间">
              {getFieldDecorator('startTime',config)(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="结束时间">
              {getFieldDecorator('endTime',config)(
                <DatePicker format="YYYY-MM-DD"  placeholder="请选择"/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="优惠券描述">
              {getFieldDecorator('description',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "优惠券描述不能超过25字" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="适用人群">
              {getFieldDecorator('userType',{
                rules: [{ required: true, message: '选择不能为空!' }],
                initialValue: 1
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    userTypeList.map((item, index) => (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    ))
                  }
                  
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="状态">
              {getFieldDecorator('isshow',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>上架</Radio>
                  <Radio value={0}>下架</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        {
          this.currentType()
        }
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'GID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: '商品名称', dataIndex: 'proName', key: 'proName'
    }
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {page, limit} = this.state;
    this.setState({isload: true})
    http.post('/coupon/HtCouponDetailPro.do', {
      couponId: id,
      page: page,
      limit: limit
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="查看买类商品"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    addVisibleTY: false,
    catVisible: false,
    catData: {},
    searchform: {
      name: '',
      isShow: ''
    },
    userTypeList: [],
    localBtns: [{
      id: 50,
      name: '新增',
      isshow: false
    }, {
      id: 48,
      name: '通用新增',
      isshow: false
    }, {
      id: 51,
      name: '修改',
      isshow: false
    }, {
      id: 49,
      name: '查询',
      isshow: false
    }, {
      id: 96,
      name: '推送',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '优惠券名称', dataIndex: 'name', key: 'name', width: 200},
    {title: '优惠券描述', dataIndex: 'description', key: 'description', width: 200},
    {title: '优惠券类型', dataIndex: 'applyTypeName', key: 'applyTypeName', width: 120},
    {
      title: '领取方式',
      dataIndex: 'effective',
      key: 'effective',
      width: 100,
      align: 'center',
      render: (text, record) => (
        record.effective === 1 ? '手动领取' : '系统发放'
      )
    },
    {title: '所属门店', dataIndex: 'storeName', key: 'storeName', width: 150},
    {
      title: '状态',
      dataIndex: 'isshow',
      key: 'isshow',
      width: 100,
      align: 'center',
      render: (text, record) => (
        record.isshow === 1 ? '上架' : '下架'
      )
    },
    
    {title: '开始时间', dataIndex: 'validateStart', key: 'validateStart', width: 140},
    {title: '失效时间', dataIndex: 'validateEnd', key: 'validateEnd', width: 140},
    {title: '用户类型', dataIndex: 'userTypeName', key: 'userTypeName', width: 100},
    {
      title: '消息推送',
      dataIndex: 'ispush',
      key: 'ispush',
      width: 100,
      align: 'center',
      render: (text, record) => (
        record.ispush === 1 ? '已推送' : '未推送'
      )
    },
    {title: '优惠券创建数量', dataIndex: 'couponIndex', key: 'couponIndex', width: 120},
    {title: '优惠券剩余数量', dataIndex: 'leaveNum', key: 'leaveNum', width: 120},
    {
      title: '折扣额度',
      dataIndex: 'discountPercent',
      key: 'discountPercent',
      width: 100,
      align: 'center',
      render: (text, record) => {
        return text + '折'
      }
    },
    {
      title: '满/减',
      dataIndex: 'discountSum',
      key: 'discountSum',
      width: 200,
      align: 'center',
      render: (text, record) => {
        if (record.type === 'C') {
          return '满' + record.discountSum + '元/减' + record.discountMoney + '元'
        }
        return ''
      }
    },
    {
      title: '代金金额',
      dataIndex: 'discountCash',
      key: 'discountCash',
      width: 100,
      align: 'center',
      render: (text, record) => {
        return text + '元'
      }
    },
    {
      title: '是否叠加',
      dataIndex: 'superposition',
      key: 'superposition',
      width: 100,
      align: 'center',
      render: (text, record) => (
        text === 1 ? '叠加' : '不叠加'
      )
    },
    {
      title: '买类商品数量',
      dataIndex: 'proNum',
      key: 'proNum',
      width: 120,
      align: 'center',
      render: (text, record) => {
        if (record.type === 'B') {
          return record.proNum
        }
        return ''
      }
    },
    {
      title: '赠类商品/数量',
      dataIndex: 'complimentaryPronum',
      key: 'complimentaryPronum',
      width: 200,
      align: 'center',
      render: (text, record) => {
        if (record.type === 'B') {
          return record.proname + '/数量(' + record.complimentaryPronum + ')'
        }
        return ''
      }
    },
    
    {
      title: '买类商品',
      key: 'cat',
      width: 65,
      fixed: 'right',
      align: 'center',
      render: (text, record) => {
        if (record.type === 'B') {
          return <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
        }
        return <Button size="small" disabled><Icon type="search"/></Button>
      },
    },
    
    {
      title: '操作',
      key: 'operation',
      width: 100,
      fixed: 'right',
      align: 'center',
      render: (text,record) => {
        //record.effective === 0
        if (true) {
          //系统发放方式
          if (this.state.localBtns[1].isshow && !this.state.localBtns[4].isshow) {
            if (record.storeId === 0 && record.ispush === 1) {
              return (<span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>)
            } else if (record.storeId === 0 && record.ispush !== 1) {
              return (<span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>推送</Button>
                </span>)
            } else if (record.storeId !== 0 && record.ispush === 1) {
            return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginBottom: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>
              )
            } else {
              return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginBottom: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>推送</Button>
                </span>
              )
            }
          } else if (!this.state.localBtns[1].isshow && this.state.localBtns[4].isshow) {
            if (record.ispush === 1) {
              return (<span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>)
            } else {
              return (
                <span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">推送</Button>
                  </Popconfirm>
                </span>
              )
            }
            
          } else if (!this.state.localBtns[1].isshow && !this.state.localBtns[4].isshow) {
            if (record.ispush === 1) {
              return (
                <span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>
              )
            } else {
              return (
                <span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>推送</Button>
                </span>
              )
            }
          } else {
            if (record.storeId === 0 && record.ispush === 1) {
              return (<span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>)
            } else if (record.storeId !== 0 && record.ispush === 1) {
              return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginBottom: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>已推送</Button>
                </span>
              )
            } else if (record.storeId === 0 && record.ispush !== 1) {
              return (
                <span>
                  <Button type="danger" size="small" disabled style={{marginBottom: '10px'}}>修改</Button>
                  <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">推送</Button>
                  </Popconfirm>
                </span>
              )
            } else {
              return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginBottom: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Popconfirm title={'确定要推送消息？'} onConfirm={() => (this.handlePush(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small">推送</Button>
                  </Popconfirm>
                </span>
              )
            }
          }
        } else {
          //手动领取方式
          if (this.state.localBtns[1].isshow && !this.state.localBtns[4].isshow) {
            if (record.storeId === 0) {
              return (<span>
                  <Button type="danger" size="small" disabled style={{marginRight: '10px'}}>修改</Button>
                  <Button type="danger" size="small" disabled>不推送</Button>
                </span>)
            } else {
              return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginRight: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>不推送</Button>
                </span>
              )
            }
          } else if (this.state.localBtns[1].isshow && this.state.localBtns[4].isshow) {
            if (record.storeId === 0) {
              return (<span>
                <Button type="danger" size="small" disabled style={{marginRight: '10px'}}>修改</Button>
                <Button type="danger" size="small" disabled>不推送</Button>
              </span>)
            } else {
              return (
                <span>
                  <Popconfirm title={record.isshow === 1 ? '确定要下架么？' : '确定要上架么？' } onConfirm={() => (this.handleStatus(record))} okText="Yes" cancelText="No">
                    <Button type="danger" size="small" style={{marginRight: '10px'}}>修改</Button>
                  </Popconfirm>
                  <Button type="danger" size="small" disabled>不推送</Button>
                </span>
              )
            }
            
          } else {
            return (<span>
                <Button type="danger" size="small" disabled style={{marginRight: '10px'}}>修改</Button>
                <Button type="danger" size="small" disabled>不推送</Button>
              </span>)
          }
        }
        
      },
    }
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="优惠券名称">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入"/>
              )}
            </FormItem>
            <FormItem label="优惠券状态">
              {getFieldDecorator('isShow')(
                <Select placeholder="请选择">
                  <Option value={1}>上架</Option>
                  <Option value={0}>下架</Option>
                </Select>
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotUserTypeList () {
    //用户类型列表
    http.post('/poster/GetUserTypeDropList.do')
      .then((result) => {
        if (result.status === 1) {
          this.setState({
            userTypeList: result.data
          })
        } else {
          message.warning(result.msg);
        }
      }).catch((error) => {
        message.error(error);
      })
  }
  gotTableData () {
    const { adminId, storeId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      storeId: storeId,
      page: page,
      limit: limit,
      name: searchform.name,
      isShow: searchform.isShow
    }
    this.setState({isload: true})
    http.post('/coupon/HtCouponList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  handleAddTY = () => {
    this.setState({
      addVisibleTY: true
    });
  }
  colseAddTY = () => {
    this.setState({
      addVisibleTY: false
    });
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId } = this.props.appStore.loginUser;
    http.post('/coupon/HtCreateCoupon.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  saveAddTY = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    http.post('/coupon/HtCreateCoupon.do', {
      adminId: adminId,
      storeId: 0,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAddTY();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleStatus = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    let isshow = 1;
    if (rows.isshow === 1) {
      isshow = 0;
    } else {
      isshow = 1;
    }
    http.post('/coupon/ReplaceEffective.do', {
      adminId: adminId,
      couponId: rows.id,
      isShow: isshow
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handlePush = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/coupon/couponpush.do', {
      adminId: adminId,
      couponId: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/poster/UpdateStatus.do', {
      adminId: adminId,
      posterId: rows.id,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleExport () {
    const { storeId } = this.props.appStore.loginUser;
    window.location.href = '/order/downloadcouponlog.do?storeId=' + storeId
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotUserTypeList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      addVisibleTY,
      catVisible,
      catData,
      userTypeList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd
    };
    const addMethodsTY = {
      handleSave: this.saveAddTY,
      handleVisible: this.colseAddTY
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '优惠券管理']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}} id='fixed'>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
            <Button type="primary" onClick={() => (this.handleAddTY())} disabled={!this.state.localBtns[1].isshow}>通用新增</Button>
            <Button type="primary" icon="download" onClick={() => (this.handleExport())} disabled={!this.state.localBtns[1].isshow}>导出</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            scroll={{x: 2000}}
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisibleTY ? (
          <AddFormTY 
            {...addMethodsTY}
            modalVisible={addVisibleTY}
            userTypeList = {userTypeList}
            
          />
          ) : null}
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            userTypeList = {userTypeList}
          />
          ) : null}
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage