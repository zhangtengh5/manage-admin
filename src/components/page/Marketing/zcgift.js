import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Icon,
  message,
  Modal,
  Popconfirm,
  InputNumber,
  Radio
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SearchCoupon from '../../common/SearchCoupon/index'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      goodsData: []
    }
  }
  gotGoods = (goods) => {
    console.log(goods);
    this.setState({
      goodsData: goods
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { goodsData} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (goodsData.length <= 0) {
        message.warning('所选优惠券不能为空');
        return
      } else {
        let couponIds = '';
        goodsData.map((item, index) => (
          couponIds += item.id + ','
        ))
        const values = {
          ...fieldsValue,
          couponIds: couponIds.substring(0, (couponIds.length - 1))
        }
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="注册大礼包新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="礼包编码">
              {getFieldDecorator('code',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: "1001"
              })(
                < Input placeholder = "请输入" disabled/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="礼包名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: "注册大礼包"
              })(
                < Input placeholder = "请输入" disabled/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="赠送金额">
              {getFieldDecorator('sendMoney', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "例如9元输入9"
                  allowClear
                  style={{width: '180px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="有效天数">
              {getFieldDecorator('days', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "例如9天输入9"
                  allowClear
                  style={{width: '180px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
      <div style={{height: 500}}>
      <SearchCoupon placeholder="请搜索" callback={this.gotGoods}/>
     </div>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: 'ID', width: 100, dataIndex: 'id', key: 'id'
    },
    {
      title: '优惠券ID', width: 100, dataIndex: 'couponId', key: 'couponId'
    },
    {
      title: '优惠券名称', dataIndex: 'couponName', key: 'couponName'
    },
    {
      title: '优惠券描述', dataIndex: 'description', key: 'description'
    },
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {limit, page} = this.state;
    this.setState({isload: true})
    http.post('/gifts/SelectItemById.do', {
      id: id,
      limit: limit,
      page: page
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="优惠券列表"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}

@Form.create() @inject('appStore') @observer
class AddFormS extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      goodsData: [],
      type: 1
    }
  }
  gotGoods = (goods) => {
    console.log(goods);
    this.setState({
      goodsData: goods
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { goodsData} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (goodsData.length <= 0) {
        message.warning('所选优惠券不能为空');
        return
      } else {
        const values = {
          ...fieldsValue,
          couponidjson: JSON.stringify(goodsData)
        }
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="邀请大礼包新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="礼包类型">
              {getFieldDecorator('type',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>完善个人信息</Radio>
                  <Radio value={2}>绑定掌静脉</Radio>
                  <Radio value={3}>充值礼包</Radio>
                  <Radio value={4}>下单礼包</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="礼包名称">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ""
              })(
                < Input placeholder = "请输入"/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="有效天数">
              {getFieldDecorator('days', {
                initialValue: '',
                rules: [{ required: true, message: '请输入正整数!'}],
              })(<InputNumber 
                  min={1}
                  placeholder = "例如9天输入9"
                  allowClear
                  style={{width: '180px'}}
                  formatter = {
                    (value) => {
                      if (value && !isNaN(value)) {
                        return parseInt(value)
                      } else {
                        return ''
                      }
                    }
                  }
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
      <div style={{height: 500}}>
      <SearchCoupon placeholder="请搜索" callback={this.gotGoods}/>
     </div>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class CatTableS extends Component {
  static defaultProps = {
    handleVisible: () => {},
    values: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      tableData: [],
      isload: false,
      limit: 10,
      page: 1,
      total: 0
    }
  }
  columns = [
    {
      title: '优惠券ID', width: 100, dataIndex: 'couponid', key: 'couponid'
    },
    {
      title: '优惠券名称', dataIndex: 'name', key: 'name'
    },
    {
      title: '优惠券类型', dataIndex: 'applyTypeName', key: 'applyTypeName'
    },
    {
      title: '优惠券描述', dataIndex: 'description', key: 'description'
    },
    {
      title: '所属店铺', dataIndex: 'storename', key: 'storename'
    },
  ]
  gotTableData() {
    const { id } = this.props.values;
    const {limit, page} = this.state;
    this.setState({isload: true})
    http.post('/invitation/SelectItemById.do', {
      id: id,
      limit: limit,
      page: page
    }).then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          isload: false,
          total: result.count
        })
      } else {
        this.setState({isload: false});
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false});
      message.error(error);
    })
  }
  componentDidMount () {
    this.gotTableData()
  }
  render () {
    const { tableData, isload, limit, total} = this.state;
    const { modalVisible, handleVisible } = this.props;
    return (
      <Modal
        rowKey = "id"
        width={600}
        destroyOnClose
        title="优惠券列表"
        footer = {null}
        visible={ modalVisible}
        onCancel={() => handleVisible()}
      >
        <Table 
          rowKey= "id"
          dataSource={tableData}
          columns={this.columns}
          style={styles.tableStyle}
          bordered
          loading = {isload}
          pagination = {
            {
              showSizeChanger: true,
              showQuickJumper: true,
              pageSize: limit,
              pageSizeOptions: ['10', '30', '50'],
              total: total,
              showTotal: function (total) {
                return `共 ${total} 条`;
              }
            }
          }
          onChange = {
            (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }
          }
        ></Table>
      </Modal>
    )
  }
}

@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    catVisible: false,
    modalVisible: false,
    rowData: {},
    catTableVisible: false,
    catData: {},
    searchform: {
      type: 0
    },
    page_s: 1,
    limit_s: 10,
    total_s: 0,
    tableData_s: [],
    isload_s: false,
    addVisible_s: false,
    catVisible_s: false,
    modalVisible_s: false,
    rowData_s: {},
    catTableVisible_s: false,
    catData_s: {},
    localBtns: [{
      id: 53,
      name: '新增',
      isshow: false
    }, {
      id: 54,
      name: '删除',
      isshow: false
    }, {
      id: 52,
      name: '查询',
      isshow: false
    }, {
      id: 104,
      name: '新增邀请礼包',
      isshow: false
    }, {
      id: 105,
      name: '删除邀请礼包',
      isshow: false
    }, {
      id: 106,
      name: '查看邀请详情',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '礼包编码', dataIndex: 'code', key: 'code', width: 120},
    {title: '礼包名称', dataIndex: 'name', key: 'name', width: 200},
    {
      title: '赠送金额',
      dataIndex: 'sendMoney',
      key: 'sendMoney',
      width: 120,
      align: 'center',
      render: (text, record) => {
        return text + '元'
      }
    },
    {
      title: '有效天数',
      dataIndex: 'days',
      key: 'days',
      width: 120,
      align: 'center',
      render: (text, record) => {
        return text + '天'
      }
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime'},
    {
      title: '优惠券',
      key: 'cat',
      width: 100,
      align: 'center',
      render: (text, record) => (
        <Button size="small" onClick={() => this.handleCat(record)}><Icon type="search"/></Button>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          return (
            <span>
              <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
                <Button type="danger" size="small">删除</Button>
              </Popconfirm>
            </span>
          )
        } else {
          return <Button type="danger" size="small" disabled>删除</Button>
        }
      },
    },
  ];
  gotTableData () {
    const { adminId } = this.props.appStore.loginUser;
    const { page, limit} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      limit: limit
    }
    this.setState({isload: true})
    http.post('/gifts/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }
  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  handleCat(rows) {
    this.setState({
      catVisible: true,
      catData: rows
    });
  }
  closeCat = () => {
    this.setState({
      catVisible: false,
      catData: []
    });
  }
  saveAdd = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/gifts/InsertGifts.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/gifts/DeleteGifts.do', {
      adminId: adminId,
      id: rows.id,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  columns_s = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {
      title: '礼包类型',
      key: 'type',
      width: 150,
      align: 'center',
      render: (text, record) => {
        if (record.type === 1) {
          return '完善个人信息'
        } else if (record.type === 2) {
          return '绑定掌静脉'
        } else if (record.type === 3) {
          return '充值礼包'
        } else {
          return '下单礼包'
        }
      },
    },
    {title: '礼包名称', dataIndex: 'name', key: 'name', width: 200},
    {
      title: '有效天数',
      dataIndex: 'days',
      key: 'days',
      width: 120,
      align: 'center',
      render: (text, record) => {
        return text + '天'
      }
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime'},
    {
      title: '优惠券',
      key: 'cat',
      width: 100,
      align: 'center',
      render: (text, record) => (
        <Button size="small" onClick={() => this.handleCat_s(record)}><Icon type="search"/></Button>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[4].isshow) {
          return (
            <span>
              <Popconfirm title='确定要进行此操作？' onConfirm={() => (this.handleDel_s(record))} okText="Yes" cancelText="No">
                <Button type="danger" size="small">删除</Button>
              </Popconfirm>
            </span>
          )
        } else {
          return <Button type="danger" size="small" disabled>删除</Button>
        }
      },
    },
  ];
  gotTableData_s () {

    const { page_s, limit_s} = this.state;
    let params = {
      page: page_s,
      limit: limit_s
    }
    this.setState({isload_s: true})
    http.post('/invitation/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData_s: result.data,
          total_s: result.count,
          isload_s: false
        })
      } else {
        this.setState({isload_s: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload_s: false})
      message.error(error);
    })
  }
  handleAdd_s = () => {
    this.setState({
      addVisible_s: true
    });
  }
  colseAdd_s = () => {
    this.setState({
      addVisible_s: false
    });
  }
  handleCat_s = (rows) => {
    this.setState({
      catVisible_s: true,
      catData_s: rows
    });
  }
  closeCat_s = () => {
    this.setState({
      catVisible_s: false,
      catData_s: []
    });
  }
  saveAdd_s = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/invitation/insertpackage.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd_s();
        this.gotTableData_s();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel_s = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/invitation/deletepackage.do', {
      adminId: adminId,
      id: rows.id,
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData_s();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotTableData_s();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      catVisible,
      catData,
      tableData_s,
      total_s,
      limit_s,
      isload_s,
      addVisible_s,
      catVisible_s,
      catData_s
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const catMethods = {
      handleVisible: this.closeCat,
    };
    const addMethods_s = {
      handleSave: this.saveAdd_s,
      handleVisible: this.colseAdd_s,
    };
    const catMethods_s = {
      handleVisible: this.closeCat_s,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['营销管理', '推广礼包']}/>
        <Card bordered={true} title='注册礼包数据' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
          />
          ) : null}
        {catData && Object.keys(catData).length ? (
          <CatTable
            {...catMethods}
            modalVisible={catVisible}
            values={catData}
          />
        ) : null}
        <Card bordered={true} title='邀请礼包数据' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd_s())} disabled={!this.state.localBtns[3].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData_s} 
            columns={this.columns_s}
            style={styles.tableStyle}
            bordered
            loading = {isload_s}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit_s,
                pageSizeOptions: ['10', '30', '50'],
                total: total_s,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData_s();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible_s ? (
          < AddFormS
            {...addMethods_s} 
            modalVisible={addVisible_s}
          />
          ) : null}
        {catData_s && Object.keys(catData_s).length ? (
          <CatTableS
            {...catMethods_s}
            modalVisible={catVisible_s}
            values={catData_s}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage