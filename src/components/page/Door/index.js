import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  Select,
  message,
  Modal,
  TimePicker,
  Radio,
  Popconfirm,
  Cascader,
  InputNumber
} from 'antd'
import { inject, observer } from 'mobx-react'
import moment from 'moment';
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import SingleUpload from '../../common/Upload/index'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      img: '',
      options: [{
        value: '上海',
        label: '上海',
        children: [{
          value: '上海市',
          label: '上海市',
          children: [{
            value: '静安区',
            label: '静安区',
          }, {
            value: '闵行区',
            label: '闵行区',
          }],
        }],
      }],
      ssq: []
    }
  }
  gotImage = (path) => {
    this.setState({
      img: path
    })
  }
  gotAddress = (value) => {
    this.setState({ssq: value})
  }
  validatePhone = (rule, value, callback) => {
    if (value && !(/^1\d{10}$/.test(value)) && !(/^\d{7,8}$/.test(value))) {
      callback('手机号或者固定电话不合法')
      return
    }
    callback();
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { img, ssq} = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!img) {
        message.warning('店铺图片不能为空');
        return
      } else if (ssq.length !== 3) {
        message.warning('请选择省市区');
        return
      } else if (fieldsValue.isShow === 1 && fieldsValue.whid !== 0) {
        message.warning('合作店铺请选择无仓库');
        return
      } else if (fieldsValue.isShow !== 1 && fieldsValue.whid === 0) {
        message.warning('实体店铺必须选择实体仓库');
        return
      } else {
        const values = {
          ...fieldsValue,
          img: img,
          provinceName: ssq[0],
          cityName: ssq[1],
          districtName: ssq[2],
          position: ssq[0] + ssq[1] + ssq[2] + fieldsValue.blockName + fieldsValue.detail,
          startTime: fieldsValue['startTime'].format('HH:mm'),
          endTime: fieldsValue['endTime'].format('HH:mm'),
          sendstartTime: fieldsValue['sendendTime'].format('HH:mm'),
          sendendTime: fieldsValue['sendendTime'].format('HH:mm')
        }
        if (Date.parse('2019-01-01 ' + values.startTime + ':00') >= Date.parse('2019-01-01 ' + values.endTime + ':00')) {
          message.warning('歇业时间不能小于开始营业时间');
        } else if (Date.parse('2019-01-01 ' + values.sendstartTime + ':00') >= Date.parse('2019-01-01 ' + values.sendendTime + ':00')) {
          message.warning('配送结束时间不能小于配送开始时间');
        } else {
          handleSave(values);
        }
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible,cKList} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { options, img} = this.state;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="门店类型">
              {getFieldDecorator('isShow',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 0
              })(
                <RadioGroup>
                  <Radio value={0}>实体店</Radio>
                  <Radio value={1}>合作店</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="门店名字">
              {getFieldDecorator('name',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
            <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="营业状态">
              {getFieldDecorator('isStop',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 0
              })(
                <RadioGroup>
                  <Radio value={0}>营业</Radio>
                  <Radio value={1}>歇业</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="选择仓库">
              {getFieldDecorator('whid',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 0
              })(
                <Select placeholder="请选择" style={{ width: '200px' }}>
                  {
                    cKList.map((item, index) => <Option value={item.id} key={index}>{item.whname}</Option>)
                  }
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="门店图片" required>
              <SingleUpload callback={this.gotImage} imageUrl = {img}/>
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="省市区" required>
              <Cascader options={options} onChange={this.gotAddress} placeholder="请选择" style={{width: '300px'}}/>
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="所属街道">
              {getFieldDecorator('blockName',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="门牌号码">
              {getFieldDecorator('detail',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="经度x轴">
              {getFieldDecorator('xNum',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ""
              })(
                <InputNumber placeholder = "请输入" style={{width: '200px'}}/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="纬度y轴">
              {getFieldDecorator('yNum',{
                rules: [
                  { required: true, message: '不能为空!' }
                ],
                initialValue: ""
              })(
                <InputNumber placeholder = "请输入" style={{width: '200px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="联系人员">
              {getFieldDecorator('contacts',{
                rules: [{ required: true, message: '不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="联系号码">
              {getFieldDecorator('phone',{
                rules: [
                  { required: true, message: '不能为空!' },
                  { validator: this.validatePhone}
                ],
                initialValue: ''
              },)(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="开始营业">
              {getFieldDecorator('startTime',{
                rules: [{ type: 'object', required: true, message: '不能为空!' }],
                initialValue: null
              })(
                <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="结束营业">
              {getFieldDecorator('endTime',{
                rules: [
                  { type: 'object', required: true, message: '不能为空!' }
                ],
                initialValue: null
              })(
                <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="配送开始时间">
              {getFieldDecorator('sendstartTime',{
                rules: [{ type: 'object', required: true, message: '不能为空!' }],
                initialValue: null
              })(
                <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="配送结束时间">
              {getFieldDecorator('sendendTime',{
                rules: [
                  { type: 'object', required: true, message: '不能为空!' }
                ],
                initialValue: null
              })(
                <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      img: '',
      options: [{
        value: '上海',
        label: '上海',
        children: [{
          value: '上海市',
          label: '上海市',
          children: [{
            value: '静安区',
            label: '静安区',
          }, {
            value: '闵行区',
            label: '闵行区',
          }],
        }],
      }],
      ssq: [],
      cKList: []
    }
  }
  gotImage = (path) => {
    this.setState({
      img: path
    })
  }
  validatePhone = (rule, value, callback) => {
    const reg = /^1\d{10}$/
    if (!reg.test(value)) {
      callback('手机号不合法')
      return
    }
    callback();
  }
  gotAddress = (value) => {
    this.setState({
      ssq: value
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { img, ssq} = this.state;
    const { id } = this.props.editValues;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!img) {
        message.warning('店铺图片不能为空');
        return
      } else if (ssq.length !== 3) {
        message.warning('请选择省市区');
        return
      } else if (fieldsValue.isShow === 1 && fieldsValue.whid !== 0) {
        message.warning('合作店铺请选择无仓库');
        return
      } else if (fieldsValue.isShow !== 1 && fieldsValue.whid === 0) {
        message.warning('实体店铺必须选择实体仓库');
        return
      } else {
        const values = {
          ...fieldsValue,
          img: img,
          id: id,
          provinceName: ssq[0],
          cityName: ssq[1],
          districtName: ssq[2],
          position: ssq[0] + ssq[1] + ssq[2] + fieldsValue.blockName + fieldsValue.detail,
          startTime: fieldsValue['startTime'].format('HH:mm'),
          endTime: fieldsValue['endTime'].format('HH:mm'),
          sendstartTime: fieldsValue['sendstartTime'].format('HH:mm'),
          sendendTime: fieldsValue['sendendTime'].format('HH:mm')
        }
        if (Date.parse('2019-01-01 ' + values.startTime + ':00') >= Date.parse('2019-01-01 ' + values.endTime + ':00')) {
          message.warning('歇业时间不能小于开始营业时间');
        } else if (Date.parse('2019-01-01 ' + values.sendstartTime + ':00') >= Date.parse('2019-01-01 ' + values.sendendTime + ':00')) {
          message.warning('配送结束时间不能小于配送开始时间');
        } else {
          handleSave(values);
        }
      }
    });
  }
  gotCKList = (id) => {
    http.post('/category/WareHouseListById.do', {
      id: id
    })
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          cKList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentDidMount () {
    const { img, provinceName, cityName, districtName, whid} = this.props.editValues;
    let arr = [];
    arr.push(provinceName)
    arr.push(cityName)
    arr.push(districtName)
    this.gotCKList(whid)
    this.setState({
      img: img,
      ssq: arr
    })
  }
  render () {
    const { modalVisible, handleVisible, editValues} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { options, img, ssq, cKList} = this.state;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="门店类型">
                {getFieldDecorator('isShow',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.isShow
                })(
                  <RadioGroup>
                    <Radio value={0}>实体店</Radio>
                    <Radio value={1}>合作店</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="门店名字">
                {getFieldDecorator('name',{
                  rules: [{ required: true, message: '名称不能为空!' }],
                  initialValue: editValues.name
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="营业状态">
                {getFieldDecorator('isStop',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.isStop
                })(
                  <RadioGroup>
                    <Radio value={0}>营业</Radio>
                    <Radio value={1}>歇业</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="选择仓库">
                {getFieldDecorator('whid',{
                  rules: [{ required: true, message: '请选择!' }],
                  initialValue: editValues.whid
                })(
                  <Select placeholder="请选择" style={{ width: '200px' }}>
                    {
                      cKList.map((item, index) => <Option value={item.id} key={index}>{item.whname}</Option>)
                    }
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="门店图片" required>
                <SingleUpload callback={this.gotImage} imageUrl = {img}/>
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="省市区" required>
                <Cascader options={options} defaultValue = {ssq} onChange={this.gotAddress} placeholder="请选择" style={{width: '300px'}}/>
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="所属街道">
                {getFieldDecorator('blockName',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.blockName
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="门牌号码">
                {getFieldDecorator('detail',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.detail
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="经度x轴">
                {getFieldDecorator('xNum',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.xNum
                })(
                  <InputNumber placeholder = "请输入" style={{width: '200px'}}/>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="纬度y轴">
                {getFieldDecorator('yNum',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.yNum
                })(
                  <InputNumber placeholder = "请输入" style={{width: '200px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="联系人员">
                {getFieldDecorator('contacts',{
                  rules: [{ required: true, message: '不能为空!' }],
                  initialValue: editValues.contacts
                })(
                  <Input placeholder= "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="联系号码">
                {getFieldDecorator('phone',{
                  rules: [
                    { required: true, message: '不能为空!' },
                    { validator: this.validatePhone}
                  ],
                  initialValue: editValues.phone
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="开始营业">
                {getFieldDecorator('startTime',{
                  rules: [{ type: 'object', required: true, message: '不能为空!' }],
                  initialValue: moment(editValues.startTime, 'HH:mm')
                })(
                  <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="结束营业">
                {getFieldDecorator('endTime',{
                  rules: [
                    { type: 'object', required: true, message: '不能为空!' }
                  ],
                  initialValue: moment(editValues.endTime, 'HH:mm')
                })(
                  <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="配送开始时间">
                {getFieldDecorator('sendstartTime',{
                  rules: [{ type: 'object', required: true, message: '不能为空!' }],
                  initialValue: moment(editValues.sendstartTime, 'HH:mm')
                })(
                  <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="配送结束时间">
                {getFieldDecorator('sendendTime',{
                  rules: [
                    { type: 'object', required: true, message: '不能为空!' }
                  ],
                  initialValue: moment(editValues.sendendTime, 'HH:mm')
                })(
                  <TimePicker placeholder = "请选择" style={{width: '200px'}} format={'HH:mm'}/>
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      name: ""
    },
    cKList: [],
    localBtns: [{
      id: 89,
      name: '新增',
      isshow: false
    }, {
      id: 91,
      name: '编辑',
      isshow: false
    }, {
      id: 90,
      name: '删除',
      isshow: false
    }, {
      id: 92,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {
      title: '门店类型',
      dataIndex: 'isShow',
      key: 'isShow',
      width: 100,
      render: (text, record) => {
        if (record.isShow === 0) {
          return '实体店'
        } else if (record.isShow === 1) {
          return '合作店'
        }
      }
    },
    {title: '门店名字', dataIndex: 'name', key: 'name', width: 250},
    {
       title: '状态',
       dataIndex: 'isStop',
       key: 'isStop',
       width: 100,
       render: (text, record) => {
         if (record.isStop === 0) {
           return '营业'
         } else if (record.isStop === 1) {
           return '不营业'
         }
       }
     },
    {title: '营业时间', dataIndex: 'startTime', key: 'startTime', width: 120},
    {title: '歇业时间', dataIndex: 'endTime', key: 'endTime', width: 120},
    {title: '配送开始时间', dataIndex: 'sendstartTime', key: 'sendstartTime', width: 140},
    {title: '配送结束时间', dataIndex: 'sendendTime', key: 'sendendTime', width: 140},
    {title: '联系人', dataIndex: 'contacts', key: 'contacts', width: 150},
    {title: '联系电话', dataIndex: 'phone', key: 'phone', width: 150},
    {
      title: '门店照片',
      dataIndex: 'img',
      key: 'img',
      width: 40,
      align: 'center',
      render: (img) => (
        <img src={img} style={{width: "40px", height: "22px"}} alt=""/>
      )
    },
    {title: '门店位置', dataIndex: 'position', key: 'position', width: 300},
    {title: '经度', dataIndex: 'xNum', key: 'xNum', width: 150},
    {title: '维度', dataIndex: 'yNum', key: 'yNum', width: 150},
    {
      title: '仓库名字',
      dataIndex: 'whName',
      key: 'whName',
      width: 300,
      render: (text, record) => {
        return record.whName ? record.whName : '无'
      }
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 150},
    {
      title: '操作',
      key: 'operation',
      width: 100,
      fixed: 'right',
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[2].isshow) {
          return <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginBottom: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
            <Popconfirm title='确定要进行此操作么？' onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
              <Button type="danger" size="small">删除</Button>
            </Popconfirm>
          </span>
        } else {
          return <span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)} style={{marginBottom: '10px'}} disabled={!this.state.localBtns[1].isshow}>编辑</Button>
            <Button type="danger" size="small" disabled>删除</Button>
          </span>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="门店名字">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { adminId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      adminId: adminId,
      page: page,
      limit: limit,
      name: searchform.name
    }
    this.setState({isload: true})
    http.post('/store/SelectAll.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId} = this.props.appStore.loginUser;
    http.post('/store/insert.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId } = this.props.appStore.loginUser;
    //console.log(fields)
    http.post('/store/update.do', {
      adminId: adminId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/store/delete.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotCKList = () => {
    http.post('/category/WareHouseListById.do', {
      id: 0
    })
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          cKList: result.data
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
    this.gotCKList();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData,
      cKList
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd,
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['系统管理','门店管理']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            scroll={{x: 1800}}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
            cKList= {cKList}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage