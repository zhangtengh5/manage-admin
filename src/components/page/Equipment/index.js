import React, { Component } from 'react';
import {
  Card,
  Table,
  BackTop,
  Input,
  Form,
  Row,
  Col,
  Button,
  message,
  Modal,
  Radio
} from 'antd'
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
import CustomBreadcrumb from '../../common/CustomBreadcrumb/index'
import PicturesWall from '../../common/Upload/multiple_nolimit'
import TypingCard from '../../common/TypingCard'
import './index.scss'
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const { TextArea } = Input;
@Form.create() @inject('appStore') @observer
class AddForm extends Component {
  static defaultProps = {
    handleSave: () => {},
    handleVisible: () => {}
  }
  constructor(props) {
    super(props)
    this.state = {
      listImg: []
    }
  }
  gotImage = (list) => {
    this.setState({
      listImg: list
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { listImg } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!listImg.length) {
        message.warning('轮播图不能为空');
        return
      } else {
        let images = listImg.map((item, index) => item.response.path)
        const values = {
          ...fieldsValue,
          images: images.join(','),
          image: ''
        }
        handleSave(values);
      }
    });
    
  }
  render () {
    const { modalVisible, handleVisible} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { listImg } = this.state;
    const { storeName} = this.props.appStore.loginUser;
    return (
      <Modal
      destroyOnClose
      width={800}
      title="新增"
      visible={modalVisible}
      onOk={() => this.okHandle()}
      onCancel={() => handleVisible()}
    > 
     <Form layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="设备名称">
              {getFieldDecorator('equipmentName',{
                rules: [{ required: true, message: '名称不能为空!' }],
                initialValue: ''
              })(
                <Input placeholder = "请输入" allowClear/ >
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="所属门店">
              {getFieldDecorator('storeName',{
                rules: [{ required: true}],
                initialValue: storeName
              })(
                <Input placeholder = "请输入" disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="设备描述" >
              {getFieldDecorator('description',{
                rules: [{ required: true, message: '内容不能为空!' }],
                initialValue: ""
              })(
                <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={12}>
            <FormItem label="状态">
              {getFieldDecorator('status',{
                rules: [{ required: true, message: '请选择!' }],
                initialValue: 1
              })(
                <RadioGroup>
                  <Radio value={1}>激活</Radio>
                  <Radio value={0}>未激活</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col span={24}>
            <FormItem label="轮播图片" required>
              <PicturesWall callback={this.gotImage} fileList = {listImg}/>
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>
    )
  }
}
@Form.create() @inject('appStore') @observer
class EditTable extends Component {
  static defaultProps = {
    handleVisible: () => {},
    handleSave: () => {},
    editValues: {},
  };
  constructor (props) {
    super(props)
    this.state = {
      listImg: []
    }
  }
  gotImage = (list) => {
    this.setState({
      listImg: list
    })
  }
  okHandle = () => {
    const { form, handleSave} = this.props;
    const { listImg } = this.state;
    const { id } = this.props.editValues;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (!listImg.length) {
        message.warning('轮播图不能为空');
        return
      } else {
        let images = listImg.map((item, index) => {
          return item.url || item.response.path
        })
        const values = {
          ...fieldsValue,
          images: images.join(','),
          image: '',
          id: id
        }
        console.log(images)
        handleSave(values);
      }
    });
  }
  componentWillMount () {
    
  }
  componentDidMount () {
    const { images } = this.props.editValues;
    let i = new Date().getTime();
    let listImg = []
    if (images) {
      images.map((item, index) => {
        i++;
        listImg.push({
          uid: i,
          url: item,
          status: 'done'
        })
        return listImg
      })
      this.setState({
        listImg: listImg
      })
    }
  }
  render () {
    const { modalVisible, handleVisible, editValues} = this.props;
    const { getFieldDecorator } = this.props.form;
    const { listImg } = this.state;
    const { storeName} = this.props.appStore.loginUser;
    return (
      <Modal
        rowKey = "id"
        width={800}
        destroyOnClose
        title="编辑"
        visible={modalVisible}
        onOk={() => this.okHandle()}
        onCancel={() => handleVisible()}
      >
        <Form layout="inline">
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="设备名称">
                {getFieldDecorator('equipmentName',{
                  rules: [{ required: true, message: '名称不能为空!' }],
                  initialValue: editValues.equipmentName
                })(
                  <Input placeholder = "请输入" allowClear/ >
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="所属门店">
                {getFieldDecorator('storeName',{
                  rules: [{ required: true}],
                  initialValue: storeName
                })(
                  <Input placeholder = "请输入" disabled/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="设备描述" >
                {getFieldDecorator('description',{
                  rules: [{ required: true, message: '内容不能为空!' }],
                  initialValue: editValues.description
                })(
                  <TextArea rows={4} placeholder = "请输入" style={{resize: 'none', width: '590px'}}/>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={12}>
              <FormItem label="状态">
                {getFieldDecorator('status',{
                  rules: [{ required: true, message: '请选择!'}],
                  initialValue: editValues.status
                })(
                  <RadioGroup>
                    <Radio value={1}>激活</Radio>
                    <Radio value={0}>未激活</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
            <Col span={24}>
              <FormItem label="轮播图片" required>
                <PicturesWall callback={this.gotImage} fileList = {listImg}/>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


@Form.create() @inject('appStore') @observer
class TablePage extends Component {
  state = {
    page: 1,
    limit: 10,
    total: 0,
    tableData: [],
    isload: false,
    addVisible: false,
    editVisible: false,
    editData: {},
    searchform: {
      name: ""
    },
    cKList: [],
    localBtns: [{
      id: 75,
      name: '新增',
      isshow: false
    }, {
      id: 74,
      name: '编辑',
      isshow: false
    }, {
      id: 73,
      name: '查询',
      isshow: false
    }]
  }
  columns = [
    {title: 'ID', dataIndex: 'id', key: 'id', width: 80},
    {title: '设备名称', dataIndex: 'equipmentName', key: 'equipmentName', width: 150},
    {title: '商店名称', dataIndex: 'storeName', key: 'storeName', width: 200},
    {title: '描述', dataIndex: 'description', key: 'description'},
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: (text, record) => {
        if (record.status === 1) {
          return '激活'
        } else if (record.status === 0) {
          return '未激活'
        }
      }
    },
    {title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: 250},
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => {
        if (this.state.localBtns[1].isshow) {
          return (<span>
            <Button type="danger" size="small" onClick={() => this.handleEdit(record)}>编辑</Button>
          </span>)
        } else {
          return <Button type="danger" size="small" disabled>编辑</Button>
        }
      },
    },
    
  ];
  renderSimpleForm() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSearch} layout="inline" >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24}>
            <FormItem label="设备名称">
              {getFieldDecorator('name', {
                initialValue: ""
              })(
                <Input placeholder="请输入" />
              )}
            </FormItem>
            <FormItem >
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
  handleSearch = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        searchform: fieldsValue
      }, () => {
        this.gotTableData();
      })
    });
  }
  gotTableData () {
    const { storeId, adminId } = this.props.appStore.loginUser;
    const { page, limit, searchform} = this.state;
    let params = {
      storeId: storeId,
      adminId: adminId,
      page: page,
      limit: limit,
      name: searchform.name
    }
    this.setState({isload: true})
    http.post('/equipment/GetList.do', params)
    .then((result) => {
      if (result.code === 0) {
        this.setState({
          tableData: result.data,
          total: result.count,
          isload: false
        })
      } else {
        this.setState({isload: false})
        message.warning(result.msg);
      }
    }).catch((error) => {
      this.setState({isload: false})
      message.error(error);
    })
  }

  handleAdd = () => {
    this.setState({
      addVisible: true
    });
  }
  colseAdd = () => {
    this.setState({
      addVisible: false
    });
  }
  saveAdd = (fields) => {
    const { adminId, storeId} = this.props.appStore.loginUser;
    http.post('/equipment/CreateEquipment.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.colseAdd();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleEdit(rows) {
     this.setState({
       editVisible: true,
       editData: rows
     });
  }
  closeEdit = () => {
    this.setState({
      editVisible: false,
      editData: []
    });
  }
  saveEdit = (fields) => {
    const { adminId, storeId} = this.props.appStore.loginUser;
    http.post('/equipment/UpdateEquipment.do', {
      adminId: adminId,
      storeId: storeId,
      ...fields
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.closeEdit();
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  handleDel = (rows) => {
    const { adminId } = this.props.appStore.loginUser;
    http.post('/store/delete.do', {
      adminId: adminId,
      id: rows.id
    }).then((result) => {
      if (result.status === 1) {
        message.success(result.msg);
        this.gotTableData();
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  gotPageBtn(id) {
    const { adminId } = this.props.appStore.loginUser;
    let localBtns  = this.state.localBtns;
    http.$ajax('/admin/selectbuttonpower.do', {
      powerid: id,
      adminid: adminId
    }).then((result) => {
      if (result.status === 1) {
        let remoteBtns = result.data
        for (let i = 0; i < localBtns.length; i++) {
          for (let j = 0; j < remoteBtns.length; j++) {
            if (localBtns[i].id === remoteBtns[j].id) {
              localBtns[i].isshow = true;
            }
          }
        }
        this.setState({
          localBtns: localBtns
        })
      } else {
        message.warning(result.msg);
      }
    }).catch((error) => {
      message.error(error);
    })
  }
  componentWillMount () {
    this.gotPageBtn(this.props.match.params.pageId);
  }
  componentDidMount() {
    this.gotTableData();
  }
  render() {
    const {
      tableData,
      total,
      limit,
      isload,
      addVisible,
      editVisible,
      editData
    } = this.state;

    const addMethods = {
      handleSave: this.saveAdd,
      handleVisible: this.colseAdd
    };
    const editMethods = {
      handleSave: this.saveEdit,
      handleVisible: this.closeEdit,
    };
    return (
      <div className="page-wrapper">
        <CustomBreadcrumb arr={['设备管理', '设备列表']}/>
        <TypingCard source={this.renderSimpleForm()} height={178}/>
        <Card bordered={true} title='数据展示' style={{marginBottom: 10, minHeight: 440}}>
          <div className="handle-card">
            <Button type="primary" onClick={() => (this.handleAdd())} disabled={!this.state.localBtns[0].isshow}>新增</Button>
          </div>
          <Table 
            rowKey= "id"
            dataSource={tableData} 
            columns={this.columns}
            style={styles.tableStyle}
            bordered
            loading = {isload}
            pagination = {
              {
                showSizeChanger: true,
                showQuickJumper: true,
                pageSize: limit,
                pageSizeOptions: ['10', '30', '50'],
                total: total,
                showTotal: function (total) {
                  return `共 ${total} 条`;
                }
              }
            }
            onChange = { (pagination) => {
              this.setState({
                page: pagination.current,
                limit: pagination.pageSize
              }, () => {
                this.gotTableData();
              })
            }}
                
          ></Table>
        </Card>
        <BackTop visibilityHeight={200} style={{right: 50}}/>
        {addVisible ? (
          <AddForm 
            {...addMethods} 
            modalVisible={addVisible}
          />
          ) : null}
        {editData && Object.keys(editData).length ? (
          <EditTable
            {...editMethods}
            modalVisible={editVisible}
            editValues={editData}
          />
        ) : null}
      </div>
    )
  }
}

const styles = {
  tableStyle: {
    width: '100%'
  }
}

export default TablePage