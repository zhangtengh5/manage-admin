import React from 'react'
import {
  Icon,
  Dropdown,
  Menu,
  Modal,
  Select,
  Row,
  Col,
  Form,
  Input,
  message
} from 'antd'
import screenfull from 'screenfull'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import http from '../../../axios/index'
// import { isAuthenticated } from '../../../utils/Session'
const Option = Select.Option;
const FormItem = Form.Item;
//withRouter一定要写在前面，不然路由变化不会反映到props中去
@Form.create() @withRouter @inject('appStore') @observer
class HeaderBar extends React.Component {
  state = {
    icon: 'arrows-alt',
    visible: false,
    avatar: require('./img/04.jpg')
  }

  componentDidMount () {
    screenfull.onchange(() => {
      this.setState({
        icon: screenfull.isFullscreen ? 'shrink' : 'arrows-alt'
      })
    })
  }

  componentWillUnmount () {
    screenfull.off('change')
  }

  toggle = () => {
    this.props.onToggle()
  }
  screenfullToggle = () => {
    if (screenfull.enabled) {
      screenfull.toggle()
    }
  }
  editPassWord = () => {
    this.setState({
      visible: true
    })
  }
  okHandle = () => {
    const { form } = this.props;
    const {adminId} = this.props.appStore.loginUser;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      if (fieldsValue.NewPassword !== fieldsValue.newPassword) {
        message.warning('确认密码不一致')
        return
      }
      http.post('/admin/UpdatePassWord.do', {
        id: adminId,
        adminId: adminId,
        password: fieldsValue.password,
        NewPassword: fieldsValue.NewPassword
      }).then((result) => {
        if (result.status === 1) {
          message.success(result.msg);
          this.setState({visible: false})
          this.logout();
        } else {
          message.warning(result.msg);
        }
      }).catch((error) => {
        message.error(error);
      })
    })
  }
  logout = () => {
    window.sessionStorage.removeItem('_zgkey');
    window.sessionStorage.removeItem('_zgstore');
    window.sessionStorage.removeItem('_zgstorename');
    this.props.appStore.toggleLogin(false)
    this.props.history.push(this.props.location.pathname)
  }
  handleStoreChange = (value) => {
    //console.log(value)
    window.sessionStorage.setItem('_zgstore', value.key);
    window.sessionStorage.setItem('_zgstorename', value.label);
    this.props.appStore.setLoginInfo({storeId: value.key, storeIdName: value.label});
    window.location.reload();
  }
  render () {
    const {icon, visible, avatar} = this.state;
    const {appStore, collapsed} = this.props;
    const {name, storeId, storeData, storeName} = this.props.appStore.loginUser;
    const { getFieldDecorator } = this.props.form;
    const menu = (
      <Menu className='menu'>
        <Menu.Item><Icon type="user" />你好，{name}</Menu.Item>
        <Menu.Item><Icon type="export" /><span onClick={this.editPassWord}>修改密码</span></Menu.Item>
        <Menu.Item><Icon type="export" /><span onClick={this.logout}>退出登录</span></Menu.Item>
      </Menu>
    )
    const storeArray = (
      <Select defaultValue={{key: storeId}} labelInValue onChange={ this.handleStoreChange }>
        {
          storeData.map((item, index) => (
            <Option value={item.storeId} key={index.toString()}>
            {item.storeName}
            </Option>
          ))
        }
      </Select>
    )
    const storeArrayNo = (<span>{storeName}</span>)

    return (
      <div id='headerbar'>
        <Row>
          <Col span={12}>
            <Row>
              <Col span={4}>
                 <Icon
                  type={collapsed ? 'menu-unfold' : 'menu-fold'}
                  className='trigger'
                  onClick={this.toggle}/>
              </Col>
              <Col span={18}>
                <span style={{marginRight:'10px'}}>当前所属店铺</span>
                {appStore.storeDataLength ? storeArray : storeArrayNo}
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <div style={{lineHeight: '64px', float: 'right'}}>
              <ul className='header-ul'>
                <li><Icon type={icon} onClick={this.screenfullToggle}/></li>
                <li>
                  <Dropdown overlay={menu}>
                    <img src={avatar} alt=""/>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
        <Modal
          destroyOnClose
          width={360}
          title="修改密码"
          visible={visible}
          onOk={() => this.okHandle()}
          onCancel={() => this.setState({visible: false})}
        > 
          <Form layout="inline">
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col span={24}>
                  <FormItem label="原密码">
                    {getFieldDecorator('password',{
                      rules: [{ required: true, message: '不能为空!' }],
                      initialValue: ""
                    })(
                      <Input.Password placeholder = "请输入" style={{width: '220px'}}/>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col span={24}>
                  <FormItem label="新密码 ">
                    {getFieldDecorator('NewPassword',{
                      rules: [{ required: true, message: '不能为空!' }],
                      initialValue: ""
                    })(
                       <Input.Password placeholder = "请输入" style={{width: '220px'}}/>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
                <Col span={24}>
                  <FormItem label="确认密码">
                    {getFieldDecorator('newPassword',{
                      rules: [{ required: true, message: '不能为空!' }],
                      initialValue: ""
                    })(
                       <Input.Password placeholder = "请输入" style={{width: '210px'}}/>
                    )}
                  </FormItem>
                </Col>
              </Row>
            </Form>
        </Modal>
      </div>
    )
  }
}

export default HeaderBar