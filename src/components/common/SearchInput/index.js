import React, { Component } from 'react';
import {Select, message} from 'antd';
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
const Option = Select.Option;
@inject('appStore') @observer
class SearchInput extends Component {
  static defaultProps = {
    callback: () => {}
  }
  constructor(props) {
    super(props);
    this.timeout = null;
    this.currentValue = '';
  }
  state = {
    data: [],
    name: undefined,
  }
  
  fetch = (name) => {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.currentValue = name;
    this.timeout = setTimeout(()=>{
      this.fake(name)
    },300);
  }
  fake = (name) => {
    const {storeId} = this.props;
    let param = {
      storeId: storeId,
      name: name
    }
    http.post('/flashsale/GetProDataList.do', param)
    .then((result) => {
      if (result.code === 0) {
        if (this.currentValue === name) {
          this.setState({
            data: result.data
          })
        }
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  handleSearch = (value) => {
    this.fetch(value);
  }
  handleChange = (value) => {
    // console.log(value)
    if (value) {
      this.setState({ name: {key: value.key}}, () => {
        this.props.callback(value)
      });
    } else {
      this.setState({
        name: undefined,
        data: []
      });
      this.props.callback(value)
    }
  }
  componentDidMount () {
    console.log('挂载')
  }
  componentDidUpdate () {
    
  }
  componentWillUnmount () {
    console.log('卸载')
  }
  render() {
    const { data, name} = this.state;
    const { placeholder, style} = this.props;
    const options = data.map((item, index) => 
        (<Option 
          key={index}
          value={item.proId}
          >
        {item.proName}-现价({item.price})- 原价({item.originalPrice})
        </Option>));
    return (
      <Select
        showSearch
        value={name}
        placeholder={placeholder}
        style={style}
        defaultActiveFirstOption={false}
        showArrow={true}
        filterOption={false}
        onSearch={this.handleSearch}
        onChange={this.handleChange}
        labelInValue
        allowClear
      >
        {options}
      </Select>
    );
  }
}

export default SearchInput