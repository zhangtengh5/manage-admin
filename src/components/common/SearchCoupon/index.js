import React, { Component } from 'react';
import {
  Select,
  message,
  Table,
  Popconfirm,
  Button,
  Form
} from 'antd';
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
const Option = Select.Option;
const FormItem = Form.Item;
@Form.create() @inject('appStore') @observer
class SearchTable extends Component {
  static defaultProps = {
    callback: () => {}
  }
  constructor(props) {
    super(props);
    this.timeout = null;
    this.currentValue = '';
  }
  state = {
    data: [],
    name: undefined,
    tableSave: []
  }
  columns = [
    {
      title: '优惠券ID', width: 90, dataIndex: 'id', key: 'id'
    },
    {
      title: '优惠券名称', width: 120, dataIndex: 'name', key: 'name'
    },
    {
      title: '优惠券类型', width: 120, dataIndex: 'applyTypeName', key: 'applyTypeName'
    },
    {
      title: '优惠券描述', dataIndex: 'description', key: 'description'
    },
    {
      title: '所属店铺', width: 150, dataIndex: 'storeName', key: 'storeName'
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => (
        <span>
          <Popconfirm title="确定进行此操作?" onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
            <Button type="danger" size="small">删除</Button>
          </Popconfirm>
        </span>
      ),
    },
  ]
  fetch = (name) => {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.currentValue = name;
    this.timeout = setTimeout(()=>{
      this.fake(name)
    },300);
  }
  fake = (name) => {
    let param = {
      name: name,
      page: 1,
      limit: 999999999
    }
    http.post('/coupon/GiftCouponList.do', param)
    .then((result) => {
      if (result.code === 0) {
        if (this.currentValue === name) {
          this.setState({
            data: result.data
          })
        }
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  handleSearch = (value) => {
    this.fetch(value);
  }
  handleChange = (value) => {
    const tableData = this.state.tableSave;
    if (value) {
      this.setState({
        name: { key: value.key}
      });
      http.post('/coupon/HtCouponDetail.do', {
        couponId: value.key
      }).then((result) => {
        if (result.status === 1) {
          let json = {
            id: result.data.id,
            name: result.data.name,
            applyTypeName: result.data.applyTypeName,
            storeName: value.label[4],
            description: result.data.description
          }
          let isexist = tableData.findIndex((item) =>
            item.id === json.id
          )
          if (isexist === -1) {
            tableData.push(json)
            this.setState({
              tableSave: tableData
            }, () => {
              this.props.callback(this.state.tableSave)
            })
          }
        } else {
          message.warning(result.msg);
        }
      }).catch((error) => {
        console.log(error)
      })
    } else {
      this.setState({
        name: undefined,
        data: []
      });
    }
  }
  handleDel = (rows) => {
    const tableData = this.state.tableSave;
    this.setState({ 
      tableSave: tableData.filter(item => item.id !== rows.id)
    }, () => {
      this.props.callback(this.state.tableSave)
      this.setState({ name: undefined});
    });
  }
  render() {
    const { data, name, tableSave} = this.state;
    const { placeholder} = this.props;
    const options = data.map((item, index) => 
              (<Option 
                key={index}
                value={item.id}
               >
              {item.name}-({item.applyTypeName})- ({item.storeName}) - ({item.description})
              </Option>));
    return (
      <div>
        <Form layout="inline">
          <FormItem label="优惠券名称" style={{margin: "15px 10px"}}>
            {(<Select
                showSearch
                value={name}
                placeholder={placeholder}
                defaultActiveFirstOption={false}
                showArrow={true}
                filterOption={false}
                onSearch={this.handleSearch}
                onChange={this.handleChange}
                labelInValue
                allowClear
                style={{width: "570px"}}
              >
                {options}
              </Select>
            )}
          </FormItem>
        </Form>
        <Table
          rowKey = "id"
          columns={this.columns} 
          dataSource={tableSave}
          scroll={{ y: 400 }}
          pagination = {false}
          bordered
          style={{width: '100%'}}
        />
      </div>
    );
  }
}

export default SearchTable