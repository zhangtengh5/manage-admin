/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @LastEditors: Please set LastEditors
 * @Date: 2019-03-28 21:00:09
 * @LastEditTime: 2019-07-01 14:27:15
 */

import React, { Component } from 'react';
import { Upload, Icon, message, Modal} from 'antd';
import './index.scss';
/*
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}
*/
function beforeUpload(file) {
  const isJPG = file.type === 'image/jpeg' || 'image/png' || 'image/gif' || 'image/jpg';
  if (!isJPG) {
    message.error('图片格式不正确!');
  }
  const isLt2M = file.size / 1024 <= 200;
  if (!isLt2M) {
    message.error('图片大小不能超过200KB!');
  }
  return isJPG && isLt2M;
}
class PicturesWall extends Component {
  static defaultProps = {
    callback: () => {}
  }
  state = {
    previewVisible: false,
    previewImage: '',
    fileList: [],
    fileList1: [{
      uid: '-1',
      url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      status: 'done'
    }, {
      uid: '-2',
      url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      status: 'done'
    }]
  }
  handleCancel = () => {
    this.setState({
      previewVisible: false
    })
  }
  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  }
  handleChange = ({file, fileList}) => {
    //console.log(file)
    //console.log(fileList)
    if (file.status === 'done') {
      fileList = fileList.map((item, index) => {
        // 相关文件对象数据格式化处理
        return item;
      });
    }

    let fileLists = file.status ? [...fileList] : this.props.fileList;

    this.props.callback(fileLists);
    if (file.status === 'uploading') {
      console.log('uploading')
    }
    if (file.status === 'done') {
      if (file.response.status === 1) {
        message.success(file.response.msg);
      } else {
        message.error(file.response.msg);
      }
    }
    
  }
  render() {
    const { previewVisible, previewImage } = this.state;
    const { fileList } = this.props;
    console.log(fileList)
    const uploadButton = (
      <div>
        <Icon type="plus"/>
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    return (
      <div className="clearfix">
        <Upload
          listType="picture-card"
          className="multiple-picture"
          onPreview={this.handlePreview}
          action = "/conmon/uploadPc.do"
          name = "file"
          fileList={fileList}
          onChange={this.handleChange}
          showUploadList={true}
          beforeUpload = {beforeUpload}
        >
          {uploadButton}
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

export default PicturesWall