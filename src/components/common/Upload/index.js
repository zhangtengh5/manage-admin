/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @LastEditors: Please set LastEditors
 * @Date: 2019-03-28 21:00:09
 * @LastEditTime: 2019-07-03 10:30:16
 */

import React, { Component } from 'react';
import { Upload, Icon, message} from 'antd';
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}
/*
function beforeUpload(file) {
  const isJPG = file.type === 'image/jpeg' || 'png' || 'gif';
  if (!isJPG) {
    message.error('You can only upload JPG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJPG && isLt2M;
}
*/
class SingleUpload extends Component {
  static defaultProps = {
    callback: () => {}
  }
  state = {
    loading: false,
    imageUrl: ''
  }
  
  handleChange = (info) => {
    //console.log(info.file)
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      if (info.file.response.status === 1) {
        getBase64(info.file.originFileObj, () => {
          this.props.callback(info.file.response.path);
          message.success(info.file.response.msg);
        });
      } else {
        message.error(info.file.response.msg);
      }
      this.setState({
        loading: false
      })
    }
  }

  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const { imageUrl } = this.props;
    return (
      <div>
        <Upload
          style={{width: "128px",height: "128px"}}
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          action = "/conmon/uploadPc.do"
          name = "file"
          onChange={this.handleChange}
        >
          {imageUrl ? <img src={imageUrl} alt="avatar" style={{width: '112px', height: '112px'}}/> : uploadButton}
        </Upload>
      </div>
    );
  }
}

export default SingleUpload