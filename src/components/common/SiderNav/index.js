import React from 'react'
import { inject, observer } from 'mobx-react'
import CustomMenu from "../CustomMenu";
/*
const menus = [{
    "id": 1,
    "parentId": null,
    "name": "首页",
    "spread": "true",
    "url": "/home",
    "type": 0,
    "icon": "home",
    "createTime": "2018-06-23 11:09:29",
    "checked": false,
    "children": []
  },
  {
    "id": 2,
    "parentId": null,
    "name": "商品管理",
    "spread": "false",
    "url": "/goods",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-06-23 11:18:56",
    "checked": false,
    "children": [{
      "id": 3,
      "parentId": 2,
      "name": "商品列表",
      "spread": "false",
      "url": "/goods/list/3",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-23 11:19:28",
      "checked": false,
      "children": []
    }, {
      "id": 4,
      "parentId": 2,
      "name": "商品RFID查询",
      "spread": "false",
      "url": "/goods/rfid/4",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-23 11:19:49",
      "checked": false,
      "children": []
    }, {
      "id": 5,
      "parentId": 2,
      "name": "热搜商品列表",
      "spread": "false",
      "url": "/goods/recommend/5",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-23 11:20:09",
      "checked": false,
      "children": []
    }]
  }, {
    "id": 6,
    "parentId": null,
    "name": "营销管理",
    "spread": "false",
    "url": "/marketing",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-06-23 15:11:44",
    "checked": false,
    "children": [{
      "id": 7,
      "parentId": 6,
      "name": "促销活动",
      "spread": "false",
      "url": "/marketing/activity/7",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-26 13:44:41",
      "checked": false,
      "children": []
    }, {
      "id": 8,
      "parentId": 6,
      "name": "限时活动",
      "spread": "false",
      "url": "/marketing/time/8",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-26 13:44:44",
      "checked": false,
      "children": []
    }, {
      "id": 9,
      "parentId": 6,
      "name": "优惠券管理",
      "spread": "false",
      "url": "/marketing/coupon/9",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-26 13:45:37",
      "checked": false,
      "children": []
    }, {
      "id": 10,
      "parentId": 6,
      "name": "注册礼包",
      "spread": "false",
      "url": "/marketing/zcgift/10",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-27 17:40:12",
      "checked": false,
      "children": []
    }, {
      "id": 11,
      "parentId": 6,
      "name": "营销礼包",
      "spread": "false",
      "url": "/marketing/yxgift/11",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-27 17:49:31",
      "checked": false,
      "children": []
    }, {
      "id": 12,
      "parentId": 6,
      "name": "结算柜促销",
      "spread": "false",
      "url": "/marketing/discount/12",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-27 17:49:34",
      "checked": false,
      "children": []
    }, {
      "id": 666,
      "parentId": 6,
      "name": "充值活动",
      "spread": "false",
      "url": "/marketing/recharge/666",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-27 17:49:34",
      "checked": false,
      "children": []
    }]
  }, {
    "id": 13,
    "parentId": null,
    "name": "订单管理",
    "spread": "false",
    "url": "/order",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-06-27 17:49:37",
    "checked": false,
    "children": [{
      "id": 14,
      "parentId": 13,
      "name": "订单列表",
      "spread": "false",
      "url": "/order/list/14",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-29 09:26:26",
      "checked": false,
      "children": null
    }, {
      "id": 15,
      "parentId": 13,
      "name": "资金流水",
      "spread": "false",
      "url": "/order/moneylog/15",
      "type": 1,
      "icon": "",
      "createTime": "2018-06-29 09:27:06",
      "checked": false,
      "children": null
    }, {
      "id": 16,
      "parentId": 13,
      "name": "流水对账",
      "spread": "false",
      "url": "/order/check/16",
      "type": 1,
      "icon": "",
      "createTime": "2018-07-02 16:00:57",
      "checked": false,
      "children": null
    }]
  }, {
    "id": 17,
    "parentId": null,
    "name": "会员管理",
    "spread": "false",
    "url": "/member",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-07-02 16:01:34",
    "checked": false,
    "children": [{
      "id": 18,
      "parentId": 17,
      "name": "会员列表",
      "spread": "false",
      "url": "/member/list/18",
      "type": 1,
      "icon": "",
      "createTime": "2018-07-03 09:46:38",
      "checked": false,
      "children": null
    }]
  }, {
    "id": 19,
    "parentId": null,
    "name": "设备管理",
    "spread": "false",
    "url": "/equipment",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-07-04 18:50:48",
    "checked": false,
    "children": [{
      "id": 20,
      "parentId": 19,
      "name": "设备列表",
      "spread": "false",
      "url": "/equipment/list/20",
      "type": 1,
      "icon": "",
      "createTime": "2018-07-04 18:51:21",
      "checked": false,
      "children": null
    }]
  }, {
    "id": 21,
    "parentId": null,
    "name": "贩卖机管理",
    "spread": "false",
    "url": "/machine",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-07-30 14:54:11",
    "checked": false,
    "children": [{
      "id": 22,
      "parentId": 21,
      "name": "贩卖机",
      "spread": "false",
      "url": "/machine/list/22",
      "type": 1,
      "icon": "",
      "createTime": "2018-07-30 14:54:42",
      "checked": false,
      "children": null
    }, {
      "id": 23,
      "parentId": 21,
      "name": "贩卖机订单",
      "spread": "false",
      "url": "/machine/order/23",
      "type": 1,
      "icon": "",
      "createTime": "2018-08-30 10:47:22",
      "checked": false,
      "children": null
    }]
  }, {
    "id": 24,
    "parentId": null,
    "name": "系统管理",
    "spread": "false",
    "url": "/system",
    "type": 0,
    "icon": "appstore",
    "createTime": "2018-08-30 10:47:58",
    "checked": false,
    "children": [{
      "id": 25,
      "parentId": 24,
      "name": "员工管理",
      "spread": "false",
      "url": "/system/employee/25",
      "type": 1,
      "icon": "",
      "createTime": "2018-09-06 15:15:11",
      "checked": false,
      "children": null
    }, {
      "id": 26,
      "parentId": 24,
      "name": "角色管理",
      "spread": "false",
      "url": "/system/role/26",
      "type": 1,
      "icon": "",
      "createTime": "2018-09-06 15:15:11",
      "checked": false,
      "children": null
    }, {
      "id": 27,
      "parentId": 24,
      "name": "门店管理",
      "spread": "false",
      "url": "/system/door/27",
      "type": 1,
      "icon": "",
      "createTime": "2018-09-06 15:15:11",
      "checked": false,
      "children": null
    }, {
      "id": 28,
      "parentId": 24,
      "name": "APP版本管理",
      "spread": "false",
      "url": "/system/appversion/28",
      "type": 1,
      "icon": "",
      "createTime": "2018-09-06 15:15:11",
      "checked": false,
      "children": null
    }, {
      "id": 29,
      "parentId": 24,
      "name": "操作日志",
      "spread": "false",
      "url": "/system/dolog/29",
      "type": 1,
      "icon": "",
      "createTime": "2018-09-06 15:15:11",
      "checked": false,
      "children": null
    }]
  }
]
*/
@inject('appStore') @observer
class SiderNav extends React.Component {
  
  componentDidMount () {
    
  }
  render() {
    const { Menus } = this.props.appStore;
    //console.log(Menus)
    return (
      <div style={{height: '100vh',overflowY:'scroll'}}>
        <div style={styles.logo}></div>
        <CustomMenu menus={Menus}/>
      </div>
    )
  }
}

const styles = {
  logo: {
    height: '32px',
    background: 'rgba(255, 255, 255, .2)',
    margin: '16px'
  }
}

export default SiderNav