import React, { Component } from 'react';
import {
  Select,
  message,
  Table,
  Popconfirm,
  Button,
  Form
} from 'antd';
import { inject, observer } from 'mobx-react'
import http from '../../../axios/index'
const Option = Select.Option;
const FormItem = Form.Item;
@Form.create() @inject('appStore') @observer
class SearchTable extends Component {
  static defaultProps = {
    callback: () => {}
  }
  constructor(props) {
    super(props);
    this.timeout = null;
    this.currentValue = '';
  }
  state = {
    data: [],
    name: undefined,
    tableSave: []
  }
  columns = [
    {
      title: 'GID', width: 100, dataIndex: 'proId', key: 'proId'
    },
    {
      title: '商品名称', dataIndex: 'proName', key: 'proName'
    },
    {
      title: '原价', width: 150, dataIndex: 'originalPrice', key: 'originalPrice'
    },
    {
      title: '现价', width: 150, dataIndex: 'price', key: 'price'
    },
    {
      title: '操作',
      key: 'operation',
      width: 100,
      align: 'center',
      render: (text,record) => (
        <span>
          <Popconfirm title="确定进行此操作?" onConfirm={() => (this.handleDel(record))} okText="Yes" cancelText="No">
            <Button type="danger" size="small">删除</Button>
          </Popconfirm>
        </span>
      ),
    },
  ]
  fetch = (name) => {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    this.currentValue = name;
    this.timeout = setTimeout(()=>{
      this.fake(name)
    },300);
  }
  fake = (name) => {
    const {storeId} = this.props;
    let param = {
      storeId: storeId,
      name: name
    }
    http.post('/flashsale/GetProDataList.do', param)
    .then((result) => {
      if (result.code === 0) {
        if (this.currentValue === name) {
          this.setState({
            data: result.data
          })
        }
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  emit = (msg) => {
    return () => {
      
    }
  }
  handleSearch = (value) => {
    this.fetch(value);
  }
  handleChange = (value) => {
    const tableData = this.state.tableSave;
    if (value) {
      let json = {
        proId: value.key,
        proName: value.label[0],
        originalPrice: value.label[4],
        price: value.label[2]
      }
      this.setState({
        name: { key: value.key}
      });
      let isexist = tableData.findIndex((item) =>
        item.proId === json.proId
      )
      if (isexist === -1) {
        tableData.push(json)
        this.setState({
          tableSave: tableData
        }, () => {
          this.props.callback(this.state.tableSave)
        })
      }
    } else {
      this.setState({
        name: undefined,
        data: []
      });
    }
  }
  handleDel = (rows) => {
    const tableData = this.state.tableSave;
    this.setState({ 
      tableSave: tableData.filter(item => item.proId !== rows.proId)
    }, () => {
      this.props.callback(this.state.tableSave)
      this.setState({ name: undefined});
    });
  }
  render() {
    const { data, name, tableSave} = this.state;
    const { placeholder} = this.props;
    const options = data.map((item, index) => 
              (<Option 
                key={index}
                value={item.proId}
                data-name = {item.proName}
                data-oldprice = {item.originalPrice}
                data-price = {item.price}
               >
              {item.proName}-现价({item.price})- 原价({item.originalPrice})
              </Option>));
    return (
      <div>
        <Form layout="inline">
          <FormItem label="搜索商品" style={{margin: "20px 10px"}}>
            {(<Select
                showSearch
                value={name}
                placeholder={placeholder}
                defaultActiveFirstOption={false}
                showArrow={true}
                filterOption={false}
                onSearch={this.handleSearch}
                onChange={this.handleChange}
                labelInValue
                allowClear
                style={{width: "570px"}}
              >
                {options}
              </Select>
            )}
          </FormItem>
        </Form>
        <Table
          rowKey = "proId"
          columns={this.columns} 
          dataSource={tableSave}
          scroll={{ y: 400 }}
          pagination = {false}
          bordered
          style={{width: '100%'}}
        />
      </div>
    );
  }
}

export default SearchTable