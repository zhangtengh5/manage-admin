import React from 'react'
import { withRouter, Switch, Redirect } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { message } from 'antd'
import http from '../axios/index'
import LoadableComponent from '../utils/LoadableComponent'
import PrivateRoute from './router'

const Home = LoadableComponent(()=>import('../components/page/Home/index'))  //参数一定要是函数，否则不会懒加载，只会代码拆分
const GoodsList = LoadableComponent(() => import('../components/page/Goods/index'));
const GoodsRfid = LoadableComponent(() => import('../components/page/Goods/rfid'));
const Recommend = LoadableComponent(() => import('../components/page/Goods/recommend'));
const ActivityList = LoadableComponent(() => import('../components/page/Activity/index'));
const ActivityTime = LoadableComponent(() => import('../components/page/Activity/time'));
const Dolog = LoadableComponent(() => import('../components/page/Dolog/index'));
const Coupon = LoadableComponent(() => import('../components/page/Marketing/coupon'));
const Discount = LoadableComponent(() => import('../components/page/Marketing/discount'));
const YXgift = LoadableComponent(() => import('../components/page/Marketing/yxgift'));
const ZCgift = LoadableComponent(() => import('../components/page/Marketing/zcgift'));
const Recharge = LoadableComponent(() => import('../components/page/Marketing/recharge'));
const Order = LoadableComponent(() => import('../components/page/Order/index'));
const Member = LoadableComponent(() => import('../components/page/Member/index'));
const Zjls = LoadableComponent(() => import('../components/page/Member/zjls'));
const Check = LoadableComponent(() => import('../components/page/Member/check'));
const Door = LoadableComponent(() => import('../components/page/Door/index'));
const Equipment = LoadableComponent(() => import('../components/page/Equipment/index'));
const Code = LoadableComponent(() => import('../components/page/Code/index'));
const Role = LoadableComponent(() => import('../components/page/System/role'));
const Employee = LoadableComponent(() => import('../components/page/System/employee'));
const Machine = LoadableComponent(() => import('../components/page/Machine/index'));
const MachineOrder = LoadableComponent(() => import('../components/page/Machine/order'));
const NoMatch = LoadableComponent(() => import('../components/page/NoMatch/index'));
// const Test = LoadableComponent(() => import('../components/page/Test/index'));
@withRouter @inject('appStore') @observer
class ContentMain extends React.Component {
  componentWillMount() {

  }
  componentDidMount() {
    let id = window.sessionStorage.getItem('_zgkey');
    let storeId = window.sessionStorage.getItem('_zgstore');
    let storeName = window.sessionStorage.getItem('_zgstorename');
    if (id) {
      let info = {
        adminId: id,
        storeId: parseInt(storeId),
        storeName: storeName
      }
      this.props.appStore.setLoginInfo(info);
      this.gotLoginInfo(id);
      this.gotTree(id);
    } else {
      this.props.history.push('/login')
    }
  }
  gotLoginInfo (id) {
    http.post('/admin/SelectById.do', {
      id: id,
    })
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        if (data.storeId.indexOf(',') !== -1) {
          this.gotStore(id, data);
        }
        let info = {
          name: data.name
        }
        this.props.appStore.setLoginInfo(info);
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  gotStore(id) {
    http.post('/admin/SelectAllStoreByAdminId.do', {
      adminId: id,
    })
    .then((result) => {
      if (result.status === 1) {
        let info = {
          storeData: result.data
        }
        this.props.appStore.setLoginInfo(info)
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  gotTree(id) {
    http.post('/admin/SelectRoleByLogin.do', {
      id: id
    })
    .then((result) => {
      if (result.status === 1) {
        //console.log(result.data)
        this.props.appStore.setMenus(result.data)
      } else {
        message.warning(result.msg);
      }
    })
    .catch((error) => {
      message.error(error);
    })
  }
  render () {
    return (
      <div style={{padding: 16, position: 'relative'}}>
        <Switch>
          <PrivateRoute  path='/home' component={Home}/>
          <PrivateRoute  path='/goods/list/:pageId' component={GoodsList}/>
          <PrivateRoute  path='/goods/rfid/:pageId' component={GoodsRfid}/>
          <PrivateRoute  path='/goods/recommend/:pageId' component={Recommend}/>
          <PrivateRoute  path='/marketing/activity/:pageId' component={ActivityList}/>
          <PrivateRoute  path='/marketing/time/:pageId' component={ActivityTime}/>
          <PrivateRoute  path='/marketing/coupon/:pageId' component={Coupon}/>
          <PrivateRoute  path='/marketing/discount/:pageId' component={Discount}/>
          <PrivateRoute  path='/marketing/yxgift/:pageId' component={YXgift}/>
          <PrivateRoute  path='/marketing/zcgift/:pageId' component={ZCgift}/>
          <PrivateRoute  path='/marketing/recharge/:pageId' component={Recharge}/>
          <PrivateRoute  path='/order/list/:pageId' component={Order}/>
          <PrivateRoute  path='/order/moneylog/:pageId' component={Zjls}/>
          <PrivateRoute  path='/order/check/:pageId' component={Check}/>
          <PrivateRoute  path='/member/list/:pageId' component={Member}/>
          <PrivateRoute  path='/equipment/list/:pageId' component={Equipment}/>
          <PrivateRoute  path='/machine/list/:pageId' component={Machine}/>
          <PrivateRoute  path='/machine/order/:pageId' component={MachineOrder}/>
          <PrivateRoute  path='/system/employee/:pageId' component={Employee}/>
          <PrivateRoute  path='/system/role/:pageId' component={Role}/>
          <PrivateRoute  path='/system/door/:pageId' component={Door}/>
          <PrivateRoute  path='/system/appversion/:pageId' component={Code}/>
          <PrivateRoute  path='/system/dolog/:pageId' component={Dolog}/>
          {/* <PrivateRoute  path='/test' component={Test}/> */}
          <Redirect exact from='/' to='/home'/>
          <PrivateRoute component={NoMatch}/>
        </Switch>
      </div>
    )
  }
}

export default ContentMain