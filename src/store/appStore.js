import {observable, action, computed} from 'mobx'
import {isAuthenticated,authenticateSuccess,logout} from '../utils/Session'

class AppStore {
  @observable isLogin = !!isAuthenticated()  //利用cookie来判断用户是否登录，避免刷新页面后登录状态丢失
  @observable users = []  //模拟用户数据库
  //当前登录用户信息
  @observable loginUser = {
    storeData: [],
    name: '',
    adminId: 0,
    storeId: 0,
    storeName: ''
  }  
  @observable isload = false
  @observable Menus = []
  @action toggleLogin(flag,info={}) {
    if (flag) {
      authenticateSuccess(info.userId)
      this.isLogin = true
    } else {
      logout()
      this.isLogin = false
    }

  }
  @action setLoginInfo(info = {}) {
     //设置登录用户信息
    (info.name) && (this.loginUser.name = info.name);
    (info.adminId) && (this.loginUser.adminId = info.adminId);
    (info.storeId) && (this.loginUser.storeId = info.storeId);
    (info.storeName) && (this.loginUser.storeName = info.storeName);
    (info.storeData) && (this.loginUser.storeData = info.storeData);
  }
  @action setMenus(info) {
    //设置菜单栏信
    this.Menus = info
  }
  @action setLoading(info) {
    //设置loading
    this.isload = info;
  }
  @action initUsers() {
    const localUsers = localStorage['users'] ? JSON.parse(localStorage['users']) : []
    this.users = [{username: 'admin', password: 'admin'},...localUsers]
  }

  @computed
  get storeDataLength () {
    return this.loginUser.storeData.length;
  }
}

export default new AppStore()